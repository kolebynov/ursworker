﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UrsWorker.Parser.Models;

namespace UrsWorker.Parser.Abstractions
{
    /// <summary>
    /// Export URS modifications to file
    /// </summary>
    public interface IUrsModificationsExporter
    {
        Task ExportAsync(string exportFileName, IEnumerable<UrsModification> changes);
        Task ExportAsync(Stream stream, IEnumerable<UrsModification> changes);
    }
}
