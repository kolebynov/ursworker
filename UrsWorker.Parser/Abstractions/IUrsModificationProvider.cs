﻿using System.Collections.Generic;
using UrsWorker.Parser.Models;
using UrsWorker.UrsModel;

namespace UrsWorker.Parser.Abstractions
{
    public interface IUrsModificationProvider
    {
        /// <summary>
        /// Return modifications from URS model
        /// </summary>
        /// <param name="ursModel">Model</param>
        /// <returns></returns>
        List<UrsModification> GetModifications(
            IUrsModel ursModel,
            bool includeInitialConfig = true,
            bool includeProtocolAsFirstRow = false
        );
    }
}
