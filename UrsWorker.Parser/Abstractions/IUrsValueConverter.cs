﻿using System;

namespace UrsWorker.Parser.Abstractions
{
    internal interface IUrsValueConverter
    {
        /// <summary>
        /// Convert string from URS to type we want
        /// </summary>
        object ConvertFromString(string value, Type conversionType);
    }
}
