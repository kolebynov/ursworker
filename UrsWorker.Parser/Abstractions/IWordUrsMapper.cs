﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using UrsWorker.Parser.Models;
using UrsWorker.UrsModel;

namespace UrsWorker.Parser.Abstractions
{
    /// <summary>
    /// OpenXml elements mapper
    /// </summary>
    internal interface IWordUrsMapper
    {
        /// <summary>
        /// OpenXml table to collection of urs items 
        /// </summary>
        /// <typeparam name="T">URS item</typeparam>
        /// <param name="table">OpenXml table</param>
        /// <param name="headerRowIndex">Header row index.</param>
        /// <param name="cellParsingHandlers"></param>
        /// <returns></returns>
        IEnumerable<T> TableToUrsModel<T>(Table table, int headerRowIndex = 0, IEnumerable<Action<ParseCellContext>> cellParsingHandlers = null)
            where T : IUrsItem, new();

        /// <summary>
        /// Get string from OpenXmlElement and remove trash
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        string ElementToString(OpenXmlElement element);
    }
}
