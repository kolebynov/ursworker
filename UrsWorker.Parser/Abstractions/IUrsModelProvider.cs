﻿using System.IO;
using System.Threading.Tasks;
using UrsWorker.UrsModel;

namespace UrsWorker.Parser.Abstractions
{
    public interface IUrsModelProvider
    {
        /// <param name="data">URS</param>
        /// <returns></returns>
        Task<IUrsModel> GetAsync(Stream data);
    }
}
