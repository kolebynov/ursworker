﻿using System.Collections.Generic;
using UrsWorker.UrsModel.Common;

namespace UrsWorker.Parser.Settings
{
    public class ParserSettings
    {
        /// <summary>
        /// Color of word in the implementation row
        /// </summary>
        public string ImplementationTypeColor { get; set; }

        public ParagraphNamesMapping ParagraphNamesMapping { get; set; }

        public ParagraphNumbersMapping ParagraphNumbersMapping { get; set; }

        public string[] TrashInImplementationRow { get; set; }

        public Dictionary<string, string> DataTransferMappings { get; set; }

        public UrsValueConvertSettings UrsValueConvertSettings { get; set; }

        public StudyInfoSettings StudyInfoSettings { get; set; }

        /// <summary>
        /// Drug mapping urs words to blindings/unblindings
        /// </summary>
        public Dictionary<string, Blinding> DrugsBlindingMapping { get; set; }
    }
}
