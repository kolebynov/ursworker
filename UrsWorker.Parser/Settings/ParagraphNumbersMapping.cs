﻿using UrsWorker.Parser.Models;

namespace UrsWorker.Parser.Settings
{
    public class ParagraphNumbersMapping
    {
        public ParagraphNumber SectionsStart { get; set; }
        public ParagraphNumber SettingsParagraphStart { get; set; }
        public ParagraphNumber SettingsParagraphEnd { get; set; }
        public ParagraphNumber VisitSchedulesParagraph { get; set; }
    }
}
