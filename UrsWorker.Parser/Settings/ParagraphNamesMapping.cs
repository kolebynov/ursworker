﻿namespace UrsWorker.Parser.Settings
{
    public class ParagraphNamesMapping
    {
        public string DataTransfers { get; set; }
        public string Countries { get; set; }
        public string StudyParameters { get; set; }
        public string Treatments { get; set; }
        public string SerializedDrugs { get; set; }
        public string BulkDrugs { get; set; }
        public string KeywordReplacements { get; set; }
        public string VersionSummary { get; set; }
        public string UnblindedDataPoints { get; set; }
        public string RandomizationSpecification { get; set; }
    }
}
