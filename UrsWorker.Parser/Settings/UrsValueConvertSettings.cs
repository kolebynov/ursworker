﻿using System.Collections.Generic;

namespace UrsWorker.Parser.Settings
{
    public class UrsValueConvertSettings
    {
        public string[] NullStrings { get; set; }
        public Dictionary<string, bool> StringToBoolMapping { get; set; }
    }
}
