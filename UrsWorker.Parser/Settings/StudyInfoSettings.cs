﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UrsWorker.Parser.Settings
{
    public class StudyInfoSettings
    {
        public string ProtocolPrefix { get; set; }

        public string DocumentVersionPrefix { get; set; }

        public string IrtVersionPrefix { get; set; }

        public string DocumentDatePrefix { get; set; }
    }
}
