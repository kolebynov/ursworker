﻿using System;
using System.IO;
using System.Threading.Tasks;
using UrsWorker.Parser.Abstractions;
using UrsWorker.UrsModel;

namespace UrsWorker.Parser.Extensions
{
    public static class ParserExtensions
    {
        public static async Task<IUrsModel> ParseAsync(this IUrsModelProvider ursParser, string filePath)
        {
            if (ursParser == null)
            {
                throw new ArgumentNullException(nameof(ursParser));
            }

            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read, Constants.DefaultFileBufferSize, FileOptions.Asynchronous))
            {
                return await ursParser.GetAsync(fs);
            }
        }
    }
}
