﻿using DocumentFormat.OpenXml;
using System;

namespace UrsWorker.Parser.Extensions
{
    internal static class OpenXmlExtensions
    {
        /// <summary>
        /// Get next element of <see cref="T"/>, which goes after <see cref="element"/> with next item distance limit <see cref="maxDistance"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="element">Current element</param>
        /// <param name="maxDistance">Distance limit</param>
        /// <returns></returns>
        public static T NextSibling<T>(this OpenXmlElement element, int maxDistance)
            where T : OpenXmlElement
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }

            T result = null;
            OpenXmlElement nextElement = element.NextSibling();

            for (int i = 0; i < maxDistance; ++i)
            {
                if (nextElement is T tempElement)
                {
                    result = tempElement;
                    break;
                }

                nextElement = nextElement.NextSibling();
            }

            return result;
        }

        public static OpenXmlElement NextSibling(this OpenXmlElement element, Func<OpenXmlElement, bool> predicate)
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }

            if (predicate == null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            OpenXmlElement currentElement = element;
            while (currentElement != null && !predicate(currentElement = currentElement.NextSibling()))
            { }

            return currentElement;
        }
    }
}
