﻿using System;
using UrsWorker.Parser.Abstractions;

namespace UrsWorker.Parser.Extensions
{
    internal static class ConvertExtensions
    {
        public static T ConvertToEnum<T>(this IUrsValueConverter valueConverter, string value)
        {
            if (valueConverter == null)
            {
                throw new ArgumentNullException(nameof(valueConverter));
            }

            return (T)valueConverter.ConvertFromString(value, typeof(T));
        }
    }
}
