﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UrsWorker.Parser.Models;
using UrsWorker.Parser.Resources;

namespace UrsWorker.Parser.Extensions
{
    internal static class WordUrsOpenXmlExtensions
    {
        /// <summary>
        /// Trash in URS document.
        /// </summary>
        private static readonly string[] Trash =
        {
            "REF",
            "\\h",
            "\\*",
            "MERGEFORMAT",
            "DOCPROPERTY"
        };

        /// <summary>
        /// Get paragraph number from hyperlink.
        /// </summary>
        /// <param name="hyperlink"></param>
        public static ParagraphNumber GetParagraphNumber(this Hyperlink hyperlink)
        {
            if (hyperlink == null)
            {
                throw new ArgumentNullException(nameof(hyperlink));
            }

            return new ParagraphNumber(hyperlink.ChildElements[0].GetFormattedUrsString());
        }

        /// <summary>
        /// Get paragraph name from hyperlink.
        /// </summary>
        /// <param name="hyperlink"></param>
        public static string GetParagraphName(this Hyperlink hyperlink)
        {
            if (hyperlink == null)
            {
                throw new ArgumentNullException(nameof(hyperlink));
            }

            return hyperlink.ChildElements[2].GetFormattedUrsString();
        }

        /// <param name="start">Start paragraph for search (included)</param>
        /// <param name="end">End paragraph for search (not included)</param>
        public static Paragraph GetParagraphByName(this Body body, string paragraph, ParagraphNumber start = default, ParagraphNumber end = default)
        {
            if (body == null)
            {
                throw new ArgumentNullException(nameof(body));
            }

            return body.GetParagraphByHyperlink(body.GetHyperlinkByParagraphName(paragraph, start, end));
        }

        public static Paragraph GetParagraphByHyperlink(this Body body, Hyperlink hyperlink)
        {
            if (body == null)
            {
                throw new ArgumentNullException(nameof(body));
            }

            return body.Elements<Paragraph>().FirstOrDefault(paragraph => 
                paragraph.ParagraphProperties?.ParagraphMarkRunProperties?.GetFirstChild<Strike>() == null
                && paragraph.Elements<BookmarkStart>().Any(x => x.Name.Value == hyperlink?.Anchor.Value));
        }

        public static Hyperlink GetHyperlinkByParagraphNumber(this Body body, ParagraphNumber number)
        {
            if (body == null)
            {
                throw new ArgumentNullException(nameof(body));
            }

            return body.GetHyperlinks(number, number.Increment())
                .FirstOrDefault();
        }

        /// <param name="start">Start paragraph for search (included)</param>
        /// <param name="end">End paragraph for search (not included)</param>
        public static Hyperlink GetHyperlinkByParagraphName(this Body body, string paragraph, ParagraphNumber start = default, ParagraphNumber end = default)
        {
            if (body == null)
            {
                throw new ArgumentNullException(nameof(body));
            }

            return body.GetHyperlinks(start, end)
                .FirstOrDefault(x => x.ChildElements[2].GetFormattedUrsString() == paragraph);
        }

        /// <param name="body"></param>
        /// <param name="start">Start paragraph for search (included)</param>
        /// <param name="end">End paragraph for search (not included)</param>
        public static Hyperlink[] GetHyperlinks(this Body body, ParagraphNumber start = default, ParagraphNumber end = default)
        {
            if (body == null)
            {
                throw new ArgumentNullException(nameof(body));
            }

            if (start == default)
            {
                start = ParagraphNumber.MinValue;
            }

            if (end == default)
            {
                end = ParagraphNumber.DefaultMaxValue;
            }

            return body.GetSdtContent().Where(x =>
                    x.ChildElements.Count > 1 && x.ChildElements[1] is Hyperlink hyperlink
                    && hyperlink.GetParagraphNumber().Between(start, end))
                .Select(x => (Hyperlink) x.ChildElements[1])
                .ToArray();
        }

        /// <summary>
        /// Return inner text from element without trash.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static string GetFormattedUrsString(this OpenXmlElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }

            return element.InnerText.GetFormattedUrsString();
        }

        /// <summary>
        /// Clear trash from string: 
        /// Removes illegal symbols, redundant spaces, words between illegal symbols
        /// </summary>
        /// <param name="value"></param>
        public static string GetFormattedUrsString(this string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            int minTrashIndex = int.MaxValue;
            int maxTrashIndex = int.MinValue;
            foreach (string t in Trash)
            {
                int index = value.IndexOf(t, StringComparison.Ordinal);
                if (index > -1)
                {
                    minTrashIndex = Math.Min(index, minTrashIndex);
                    maxTrashIndex = Math.Max(index + t.Length, maxTrashIndex);
                }
            }

            string preparedValue = value;
            if (minTrashIndex > -1 && maxTrashIndex > -1)
            {
                preparedValue = value.Remove(minTrashIndex, maxTrashIndex - minTrashIndex);
            }

            return preparedValue.Aggregate(new StringBuilder(), 
                (sb, ch) => ch == ' ' && (sb.Length == 0 || sb[sb.Length - 1] == ' ') ? sb : sb.Append(ch), 
                sb => (sb.Length > 0 && sb[sb.Length - 1] == ' ' ? sb.Remove(sb.Length - 1, 1) : sb).ToString()
            );
        }

        public static IEnumerable<T> GetElementsBetweenHyperlinks<T>(this Body body, Hyperlink start, Hyperlink end) where T : OpenXmlElement
        {
            if (body == null)
            {
                throw new ArgumentNullException(nameof(body));
            }

            if (start.GetParagraphNumber() >= end.GetParagraphNumber())
            {
                throw new ArgumentException(StringResources.WordUrsExtensions_StartAndEndWrong, nameof(start));
            }

            Paragraph startParagraph = body.GetParagraphByHyperlink(start);
            Paragraph endParagraph = body.GetParagraphByHyperlink(end);

            OpenXmlElement currentElement = startParagraph.NextSibling();
            List<T> result = new List<T>();
            while (currentElement != endParagraph)
            {
                if (currentElement is T rightElement)
                {
                    result.Add(rightElement);
                }

                currentElement = currentElement.NextSibling();
            }

            return result;
        }

        /// <summary>
        /// Get element which contains Table of Contents.
        /// </summary>
        private static SdtContentBlock GetSdtContent(this Body body)
        {
            if (body == null)
            {
                throw new ArgumentNullException(nameof(body));
            }

            return body.GetFirstChild<SdtBlock>()?.GetFirstChild<SdtContentBlock>();
        }
    }
}
