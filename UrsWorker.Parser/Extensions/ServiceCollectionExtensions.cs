﻿using Microsoft.Extensions.DependencyInjection;
using UrsWorker.Parser.Abstractions;
using UrsWorker.Parser.Implementations;

namespace UrsWorker.Parser.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddUrsParser(this IServiceCollection services)
        {
            services.AddScoped<IUrsModificationProvider, UrsModificationProvider>();
            services.AddScoped<IUrsModificationsExporter, ExcelUrsModificationsExporter>();
            services.AddScoped<IUrsModelProvider, WordUrsModelProvider>();

            return services;
        }
    }
}
