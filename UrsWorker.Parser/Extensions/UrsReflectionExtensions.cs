﻿using System;
using System.Linq;
using System.Reflection;
using UrsWorker.UrsModel.Attributes;

namespace UrsWorker.Parser.Extensions
{
    internal static class UrsReflectionExtensions
    {
        public static PropertyInfo GetPropertyByUrsName(this Type type, string name)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return GetMemberByUrsName(type.GetProperties(), name);
        }

        public static FieldInfo GetFieldByUrsName(this Type type, string name)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return GetMemberByUrsName(type.GetFields(), name);
        }

        /// <summary>
        /// Get member info by URS name. Get by NamesInUrsAttribute or by field/property name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="members"></param>
        /// <param name="name">URS name</param>
        /// <returns></returns>
        public static T GetMemberByUrsName<T>(this T[] members, string name) where T : MemberInfo
        {
            if (members == null)
            {
                throw new ArgumentNullException(nameof(members));
            }

            return members.FirstOrDefault(member =>
                       // Search by names from NamesInUrsAttribute first
                       member.GetCustomAttribute<NamesInUrsAttribute>()?.Names.Contains(name, StringComparer.OrdinalIgnoreCase) ?? false)
                   // Search by member names
                   ?? members.FirstOrDefault(member => member.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
        }
    }
}
