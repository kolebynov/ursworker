﻿using System;
using System.Collections.Generic;
using System.Linq;
using UrsWorker.Parser.Abstractions;
using UrsWorker.Parser.Models;
using UrsWorker.UrsModel;
using UrsWorker.UrsModel.Sections;

namespace UrsWorker.Parser.Implementations
{
    public class UrsModificationProvider : IUrsModificationProvider
    {
        private const string ChangesDelimeter = "— ";

        /// <inheritdoc />
        public List<UrsModification> GetModifications(
            IUrsModel ursModel,
            bool includeInitialConfig = true,
            bool includeProtocolAsFirstRow = false
        )
        {
            List<UrsModification> modifications = new List<UrsModification>();

            if (includeProtocolAsFirstRow)
            {
                modifications.Add(new UrsModification("Protocol", ursModel.Protocol));
            }

            if (includeInitialConfig)
            {
                modifications.Add(new UrsModification("Initial configuration", ""));
            }

            modifications.AddRange(
                ursModel.Sections
                // select Modified and New sections only
                .Where(x => x.Implementation == ImplementationType.Modified || x.Implementation == ImplementationType.New)
                // convert to format:
                // title: 6.1 Subject Page
                // description: — some modification 1
                // — some modification 2
                .Select(x => new UrsModification(
                    title: x.Number + " " + x.Name,
                    description: string.Join(
                        Environment.NewLine,
                        x.ChangeDescriptions.SelectMany(y => y.Changes.Select(c => ChangesDelimeter + c)))
                    )
                )
                // include DT 
                .Concat(ursModel.DataIntegrations
                    .Select(x => new UrsModification(x.TargetSystem, "")))
            );

            return modifications;
        }
    }
}
