﻿using System;
using System.Linq;
using System.Reflection;
using UrsWorker.Parser.Abstractions;
using UrsWorker.Parser.Extensions;
using UrsWorker.Parser.Resources;
using UrsWorker.Parser.Settings;

namespace UrsWorker.Parser.Implementations
{
    internal class UrsValueConverter : IUrsValueConverter
    {
        private readonly UrsValueConvertSettings _ursValueConvertSettings;

        public UrsValueConverter(UrsValueConvertSettings ursValueConvertSettings)
        {
            _ursValueConvertSettings = ursValueConvertSettings ?? throw new ArgumentNullException(nameof(ursValueConvertSettings));
        }

        public object ConvertFromString(string value, Type conversionType)
        {
            if (string.IsNullOrEmpty(value) || _ursValueConvertSettings.NullStrings.Contains(value, StringComparer.OrdinalIgnoreCase))
            {
                return conversionType.IsValueType ? Activator.CreateInstance(conversionType) : null;
            }

            if (conversionType == typeof(string))
            {
                return value;
            }

            if (conversionType == typeof(int) || conversionType == typeof(int?))
            {
                return int.Parse(value);
            }

            if (conversionType == typeof(bool) || conversionType == typeof(bool?))
            {
                return _ursValueConvertSettings.StringToBoolMapping[value.ToLower()];
            }

            if (conversionType.IsEnum)
            {
                return ConvertToEnum(value, conversionType);
            }

            if (conversionType == typeof(DateTime) || conversionType == typeof(DateTime?))
            {
                return DateTime.Parse(value);
            }

            throw new NotSupportedException(conversionType.ToString());
        }

        private object ConvertToEnum(string value, Type enumType)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            if (!enumType.IsEnum)
            {
                throw new ArgumentException(StringResources.ParserExtensions_ItsNotEnumType, nameof(enumType));
            }

            FieldInfo field = enumType.GetFieldByUrsName(value);
            if (field == null)
            {
                throw new ArgumentException(string.Format(StringResources.WordUrsExtensions_WrongValue, value), nameof(value));
            }

            return field.GetValue(null);
        }
    }
}
