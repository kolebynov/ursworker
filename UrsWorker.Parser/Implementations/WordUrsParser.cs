﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.Linq;
using UrsWorker.Parser.Abstractions;
using UrsWorker.Parser.Extensions;
using UrsWorker.Parser.Models;
using UrsWorker.Parser.Settings;
using UrsWorker.UrsModel;
using UrsWorker.UrsModel.Common;
using UrsWorker.UrsModel.Sections;

namespace UrsWorker.Parser.Implementations
{
    /// <summary>
    /// Parse URS from OpenXML document
    /// </summary>
    internal class WordUrsParser
    {
        private readonly Body _body;
        private readonly IWordUrsMapper _wordUrsMapper;
        private readonly IUrsValueConverter _ursValueConverter;
        private readonly ParserSettings _settings;

        public WordUrsParser(Body body, IWordUrsMapper wordUrsMapper, IUrsValueConverter ursValueConverter,
            ParserSettings settings)
        {
            _body = body ?? throw new ArgumentNullException(nameof(body));
            _wordUrsMapper = wordUrsMapper ?? throw new ArgumentNullException(nameof(wordUrsMapper));
            _ursValueConverter = ursValueConverter ?? throw new ArgumentNullException(nameof(ursValueConverter));
            _settings = settings ?? throw new ArgumentNullException(nameof(settings));
        }

        public string ParseProtocol() => _wordUrsMapper
            .ElementToString((Paragraph)_body.ChildElements.First(x => x.InnerText.StartsWith(_settings.StudyInfoSettings.ProtocolPrefix)))
            .Replace($"{_settings.StudyInfoSettings.ProtocolPrefix}", "")
            .GetFormattedUrsString();

        public string ParseVersion() => GetValueFromTitlePageTable(_settings.StudyInfoSettings.DocumentVersionPrefix);

        public string ParseIrtVersion() => GetValueFromTitlePageTable(_settings.StudyInfoSettings.IrtVersionPrefix);

        public DateTime ParseDate() => DateTime.Parse(GetValueFromTitlePageTable(_settings.StudyInfoSettings.DocumentDatePrefix));

        public bool ParseIsRandomizedStudy()
        {
            string res = _body.GetParagraphByName(_settings.ParagraphNamesMapping.RandomizationSpecification)
                .NextSibling(x => !string.IsNullOrEmpty(x.InnerText))
                .GetFormattedUrsString();

            return !_settings.UrsValueConvertSettings.NullStrings
                .Any(nullString => res.StartsWith(nullString, StringComparison.OrdinalIgnoreCase));
        }

        public IReadOnlyList<VersionSummary> ParseVersionSummaries() =>
            ParseTable<VersionSummary>(_settings.ParagraphNamesMapping.VersionSummary, default, default, 10);

        public IReadOnlyList<KeywordReplacement> ParseKeywordReplacements() =>
            ParseTable<KeywordReplacement>(_settings.ParagraphNamesMapping.KeywordReplacements);

        public IReadOnlyList<Drug> ParseDrugs() =>
            ParseTable<Drug>(
                _settings.ParagraphNamesMapping.SerializedDrugs,
                _settings.ParagraphNumbersMapping.SettingsParagraphStart,
                _settings.ParagraphNumbersMapping.SettingsParagraphEnd,
                2,
                DrugsCellParserHandler
            )
            .Concat(ParseTable<Drug>(_settings.ParagraphNamesMapping.BulkDrugs, _settings.ParagraphNumbersMapping.SettingsParagraphStart, _settings.ParagraphNumbersMapping.SettingsParagraphEnd, 2, DrugsCellParserHandler))
            .ToList().AsReadOnly();

        public IReadOnlyList<Treatment> ParseTreatments() =>
            ParseTable<Treatment>(
                _settings.ParagraphNamesMapping.Treatments,
                _settings.ParagraphNumbersMapping.SettingsParagraphStart,
                _settings.ParagraphNumbersMapping.SettingsParagraphEnd
            );

        public IReadOnlyList<string> ParseUnblindedDataPoints()
        {
            // Link to Unblinded Data Points
            Hyperlink unblindedDataPointsHyperlink = _body.GetHyperlinkByParagraphName(_settings.ParagraphNamesMapping.UnblindedDataPoints,
                _settings.ParagraphNumbersMapping.SettingsParagraphStart, _settings.ParagraphNumbersMapping.SettingsParagraphEnd);
            // Link to the next paragraph
            Hyperlink nextHyperlink =
                _body.GetHyperlinks(unblindedDataPointsHyperlink.GetParagraphNumber()).ElementAt(1);

            // Elements between two links are Unblinded Data Points
            return _body.GetElementsBetweenHyperlinks<Paragraph>(unblindedDataPointsHyperlink, nextHyperlink)
                .Skip(1) // Skip trash
                .Select(p => _wordUrsMapper.ElementToString(p))
                .Where(s => !string.IsNullOrEmpty(s))
                .ToList().AsReadOnly();
        }

        public IReadOnlyList<VisitSchedule> ParseVisitSchedules() =>
            // Get all links to visit schedules
            _body.GetHyperlinks(_settings.ParagraphNumbersMapping.VisitSchedulesParagraph, _settings.ParagraphNumbersMapping.VisitSchedulesParagraph.Increment())
                .Skip(1) // Skip trash
                .Select(hyperlink => new VisitSchedule
                {
                    Name = hyperlink.GetParagraphName(),
                    Visits = _wordUrsMapper.TableToUrsModel<Visit>(_body.GetParagraphByHyperlink(hyperlink).NextSibling<Table>())
                })
                .ToList().AsReadOnly();

        public IReadOnlyList<StudyParameter> ParseStudyParameters() =>
            ParseTable<StudyParameter>(_settings.ParagraphNamesMapping.StudyParameters, _settings.ParagraphNumbersMapping.SettingsParagraphStart, _settings.ParagraphNumbersMapping.SettingsParagraphEnd);

        public IReadOnlyList<Country> ParseCountries() =>
            ParseTable<Country>(_settings.ParagraphNamesMapping.Countries, _settings.ParagraphNumbersMapping.SettingsParagraphStart, _settings.ParagraphNumbersMapping.SettingsParagraphEnd);

        public IReadOnlyList<DataIntegration> ParseDataIntegrations()
        {
            var res = ParseTable<DataIntegration>(
                _settings.ParagraphNamesMapping.DataTransfers,
                _settings.ParagraphNumbersMapping.SettingsParagraphStart,
                _settings.ParagraphNumbersMapping.SettingsParagraphEnd
            );

            foreach (var dataIntegration in res)
            {
                dataIntegration.TargetSystem = _settings.DataTransferMappings.ContainsKey(dataIntegration.TargetSystem) ?
                    _settings.DataTransferMappings[dataIntegration.TargetSystem]
                    : dataIntegration.TargetSystem;
            }

            return res;
        }

        /// <summary>
        /// Parse all sections starts from <see cref="_settings.ParagraphNumbersMapping.SectionsStart"/>
        /// </summary>
        /// <returns></returns>
        public IReadOnlyList<Section> ParseSections()
        {
            List<Section> result = new List<Section>(100);
            Hyperlink[] hyperlinks = _body.GetHyperlinks(_settings.ParagraphNumbersMapping.SectionsStart);

            foreach (Hyperlink hyperlink in hyperlinks)
            {
                Table sectionTable = null;
                // Get element after paragraph, it should be a table
                OpenXmlElement element = _body.GetParagraphByHyperlink(hyperlink)?.NextSibling();
                if (element is Table table)
                {
                    sectionTable = table;
                }
                // Sometimes an empty string presented between paragraph and table. Check this case
                else if (element is Paragraph && string.IsNullOrEmpty(element.InnerText))
                {
                    // Get next element after empty string. It should be a table 
                    sectionTable = element.NextSibling() as Table;
                }

                // If it is a table and it starts with "Implementation" then it is right section with description
                if (sectionTable != null && sectionTable.InnerText.StartsWith("Implementation"))
                {
                    // when desription of changes header is located in the current table then it has index 3
                    int headerRowIndex = 3;
                    Table changesTable = sectionTable;
                    // check if table with desription of changes is another table
                    if (sectionTable.Elements<TableRow>().Count() < headerRowIndex && string.IsNullOrEmpty(sectionTable.NextSibling().InnerText))
                    {
                        headerRowIndex = 0;
                        changesTable = sectionTable.NextSibling<Table>();
                    }

                    IEnumerable<TableRow> sectionTableRows = sectionTable.Elements<TableRow>();

                    Section section = new Section
                    {
                        Number = hyperlink.GetParagraphNumber().ToString(),
                        Name = hyperlink.GetParagraphName(),
                        Implementation = _ursValueConverter.ConvertToEnum<ImplementationType>(
                            sectionTableRows.ElementAt(0).Elements<TableCell>().ElementAt(1).GetFirstChild<Paragraph>()
                                .First(x => !_settings.TrashInImplementationRow.Contains(x.GetFormattedUrsString()) && x.ChildElements[0] is RunProperties properties
                                    && properties.Color?.Val == _settings.ImplementationTypeColor)
                                .GetFormattedUrsString()
                        ),
                        Blinding = _ursValueConverter.ConvertToEnum<Blinding>(sectionTableRows.ElementAt(1).Elements<TableCell>().ElementAt(1).GetFormattedUrsString()),
                        ChangeDescriptions = new List<ChangeDescription>()
                    };

                    if (section.Implementation == ImplementationType.Modified
                        || section.Implementation == ImplementationType.New)
                    {
                        // This case when description of changes is located in a single cell without columns.
                        // It happens in the sections Appendix 2a, Appendix 2b
                        if (changesTable.Last().Count(x => x is TableCell) == 1)
                        {
                            section.ChangeDescriptions = new List<ChangeDescription>
                            {
                                new ChangeDescription
                                {
                                    Changes = sectionTable.Last().ChildElements[1]
                                        .Elements<Paragraph>()
                                        .Skip(1)
                                        .Select(x => x.GetFormattedUrsString())
                                        .ToList()
                                }
                            };
                        }
                        else
                        {
                            section.ChangeDescriptions = _wordUrsMapper.TableToUrsModel<ChangeDescription>(changesTable, headerRowIndex);
                        }
                    }

                    result.Add(section);
                }
            }

            return result.AsReadOnly();
        }

        /// <typeparam name="T">URS item</typeparam>
        /// <param name="paragraphName">Paragraph name. Table goes after this paragraph</param>
        /// <param name="start">Start paragraph number for search (included)</param>
        /// <param name="end">End paragraph number for search (not included)</param>
        /// <param name="maxDistanceToTable">Max amount of elements from paragraph to table</param>
        private IReadOnlyList<T> ParseTable<T>(
            string paragraphName,
            ParagraphNumber start = default,
            ParagraphNumber end = default,
            int maxDistanceToTable = 2,
            Action<ParseCellContext> parseCellHandler = null
        ) where T : IUrsItem, new()
        {
            Paragraph paragraph = _body.GetParagraphByName(paragraphName, start, end);
            Table table = paragraph?.NextSibling<Table>(maxDistanceToTable);

            return table != null
                ? _wordUrsMapper.TableToUrsModel<T>(table, 0, parseCellHandler != null ? new[] { parseCellHandler } : null).ToList().AsReadOnly()
                : new List<T>().AsReadOnly();
        }

        /// <summary>
        /// Parsing handler for URS item <see cref="Drug"/>. Used for correct parsing of <see cref="Blinding"/> from cell.
        /// <see cref="Blinding"/> in the drugs table is not the same <see cref="Blinding"/> in the sections.
        /// </summary>
        private void DrugsCellParserHandler(ParseCellContext context)
        {
            if (context.ModelProperty?.Name == nameof(Drug.Blinding))
            {
                context.ParsedValue = _settings.DrugsBlindingMapping[_wordUrsMapper.ElementToString(context.Cell.GetFirstChild<Paragraph>())];
                context.IsValueParsed = true;
            }
        }

        /// <summary>
        /// Get value from the table in the title page
        /// </summary>
        private string GetValueFromTitlePageTable(string key) => _wordUrsMapper.ElementToString(_body.GetFirstChild<Table>().LastChild
            .GetFirstChild<TableCell>()
            .Elements<Paragraph>()
            .First(p => p.InnerText.Contains(key)))
            .Replace(key, "").GetFormattedUrsString();
    }
}
