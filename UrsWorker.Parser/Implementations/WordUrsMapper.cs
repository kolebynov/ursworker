﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UrsWorker.Parser.Abstractions;
using UrsWorker.Parser.Extensions;
using UrsWorker.Parser.Models;
using UrsWorker.UrsModel;

namespace UrsWorker.Parser.Implementations
{
    internal class WordUrsMapper : IWordUrsMapper
    {
        private readonly IUrsValueConverter _valueConverter;

        public WordUrsMapper(IUrsValueConverter valueConverter)
        {
            _valueConverter = valueConverter ?? throw new ArgumentNullException(nameof(valueConverter));
        }

        public IEnumerable<T> TableToUrsModel<T>(Table table, int headerRowIndex = 0, IEnumerable<Action<ParseCellContext>> columnParsingHandlers = null)
            where T : IUrsItem, new()
        {
            if (table == null)
            {
                throw new ArgumentNullException(nameof(table));
            }

            IEnumerable<TableRow> rows = table.Elements<TableRow>()
                .Skip(headerRowIndex); // Go to header row

            TableRow headerRow = rows.First();

            object GetUrsValueFromParsedCell(ParsedCell parsedCell, List<T> list)
            {
                if (list.Count == 0 && parsedCell.Value.State == ParsedValueState.NotExist)
                {
                    return parsedCell.Value.Value;
                }

                return parsedCell.Value.State != ParsedValueState.NotExist
                    ? parsedCell.Value.Value
                    // Use value from previous model when status NotExist
                    // It happens when one row splitted on few subrows
                    //
                    //Baseline (Day 1)   |  A ...
                    //                   --------------------
                    //                   |  B ...
                    : parsedCell.Property?.GetValue(list[list.Count - 1]);
            }

            return rows.Skip(1) // Skip header row
                .Select(row => MapTableRowToUrsItem<T>(row, headerRow, columnParsingHandlers ?? new Action<ParseCellContext>[0]))
                .Aggregate(new List<T>(), (list, parsedCells) =>
                {
                    if (parsedCells.All(cell => // Remove row if all values were removed or not exist
                        cell.Value.State == ParsedValueState.NotExist || cell.Value.State == ParsedValueState.Removed))
                    {
                        return list;
                    }

                    T model = new T();
                    foreach (ParsedCell parsedCell in parsedCells)
                    {
                        object ursValue = GetUrsValueFromParsedCell(parsedCell, list);
                        if (parsedCell.Property != null)
                        {
                            parsedCell.Property.SetValue(model, ursValue);
                        }
                    }

                    list.Add(model);

                    return list;
                });
        }

        /// <inheritdoc />
        public string ElementToString(OpenXmlElement element)
        {
            return string.Join("", element.Elements<Run>()
                .Where(ValidateRun)
                .Select(r => r.InnerText)).GetFormattedUrsString();
        }

        /// <summary>
        /// Map table row to urs item
        /// </summary>
        /// <typeparam name="T">URS item</typeparam>
        /// <param name="row"></param>
        /// <param name="headerRow"></param>
        /// <param name="columnParsingHandlers"></param>
        /// <returns></returns>
        private ParsedCell[] MapTableRowToUrsItem<T>(TableRow row, TableRow headerRow, IEnumerable<Action<ParseCellContext>> columnParsingHandlers)
            where T : IUrsItem, new()
        {
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            TableCell[] rowCells = row.Elements<TableCell>().ToArray();
            TableCell[] headerCells = headerRow.Elements<TableCell>().ToArray();

            ParsedCell[] result = new ParsedCell[rowCells.Length];
 
            for (int i = 0; i < rowCells.Length; ++i)
            {
                TableCell cell = rowCells[i];
                string columnName = headerCells[i].GetFormattedUrsString();
                PropertyInfo prop = properties.GetMemberByUrsName(columnName);

                ParseCellContext context = new ParseCellContext(prop, columnName, cell);
                foreach (Action<ParseCellContext> columnParsingHandler in columnParsingHandlers)
                {
                    columnParsingHandler(context);
                }

                if (context.IsValueParsed)
                {
                    result[i] = new ParsedCell(columnName, prop, new ParsedValue(ParsedValueState.Exist, context.ParsedValue));
                }
                else if (prop != null)
                {
                    // Parse cell as collection if property type implements IEnumerable interface
                    if (prop.PropertyType != typeof(string) && prop.PropertyType.GetInterfaces().Any(x => x == typeof(IEnumerable)))
                    {
                        Type collectionItemType = prop.PropertyType.GetGenericArguments()[0];
                        IList collection = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(collectionItemType));
                        foreach (ParsedValue parsedValue in cell.Elements<Paragraph>().Select(x => GetValueFromParagraph(x, collectionItemType)).Where(x => x.Value != null))
                        {
                            collection.Add(parsedValue.Value);
                        }

                        result[i] = new ParsedCell(columnName, prop, new ParsedValue(ParsedValueState.Exist, collection));
                    }
                    else
                    {
                        result[i] = new ParsedCell(columnName, prop, GetValueFromParagraph(cell.GetFirstChild<Paragraph>(), prop.PropertyType));
                    }
                }
                else
                {
                    result[i] = new ParsedCell(columnName, null, GetValueFromParagraph(cell.GetFirstChild<Paragraph>(), typeof(string)));
                }
            }

            return result;
        }

        /// <summary>
        /// Get value from OpenXML paragraph element
        /// </summary>
        private ParsedValue GetValueFromParagraph(Paragraph element, Type valueType)
        {
            
            // If text is empty then cell is probably splitted
            if (string.IsNullOrEmpty(element.InnerText))
            {
                return new ParsedValue(ParsedValueState.NotExist, null);
            }

            string strValue = ElementToString(element);
            // Set status removed when text was crossed out
            ParsedValueState valueState = string.IsNullOrEmpty(strValue) ? ParsedValueState.Removed : ParsedValueState.Exist;
            try
            {
                return new ParsedValue(valueState, _valueConverter.ConvertFromString(strValue, valueType));
            }
            catch
            {
                //Return default value if value in urs can't be parsed.
                return new ParsedValue(ParsedValueState.Exist, valueType.IsValueType ? Activator.CreateInstance(valueType) : null);
            }
        }


        private bool ValidateRun(Run run) =>
            // Run isn't superscript
            run.RunProperties.VerticalTextAlignment?.Val?.Value != VerticalPositionValues.Superscript
            // Run isn't crossed out
            && run.RunProperties.Strike == null;

        private class ParsedCell
        {
            public ParsedValue Value { get; }

            /// <summary>
            /// Column name in table
            /// </summary>
            public string ColumnName { get; }

            /// <summary>
            /// Property from urs item, can be null
            /// </summary>
            public PropertyInfo Property { get; }

            public ParsedCell(string columnName, PropertyInfo property, ParsedValue value)
            {
                ColumnName = columnName;
                Property = property;
                Value = value;
            }

            public override string ToString() => $"Column: {ColumnName}. Property: {Property}. ParsedValue: ({Value})";
        }

        private class ParsedValue
        {
            public ParsedValueState State { get; }
            public object Value { get; }

            public ParsedValue(ParsedValueState state, object value)
            {
                State = state;
                Value = value;
            }

            public override string ToString() => $"State: {State}, Value: {Value}";
        }

        private enum ParsedValueState
        {
            /// <summary>
            /// Value exists in cell.
            /// </summary>
            Exist,

            /// <summary>
            /// Value doesn't exist in cell (cell is probably splitted).
            /// </summary>
            NotExist,

            /// <summary>
            /// Value is crossed out in cell.
            /// </summary>
            Removed
        }
    }
}
