﻿using DocumentFormat.OpenXml.Packaging;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Threading.Tasks;
using UrsWorker.Parser.Abstractions;
using UrsWorker.Parser.Models;
using UrsWorker.Parser.Settings;
using UrsWorker.UrsModel;

namespace UrsWorker.Parser.Implementations
{
    public class WordUrsModelProvider : IUrsModelProvider
    {
        private readonly IOptions<ParserSettings> _parserSettings;

        public WordUrsModelProvider(IOptions<ParserSettings> parserSettings)
        {
            _parserSettings = parserSettings ?? throw new ArgumentNullException(nameof(parserSettings));
        }

        public Task<IUrsModel> GetAsync(Stream data)
        {
            using (WordprocessingDocument document = WordprocessingDocument.Open(data, false))
            {
                var ursValueConverter = new UrsValueConverter(_parserSettings.Value.UrsValueConvertSettings);

                return Task.FromResult((IUrsModel) new WordUrsModel(
                    new WordUrsParser(document.MainDocumentPart.Document.Body,
                        new WordUrsMapper(ursValueConverter), ursValueConverter, _parserSettings.Value))
                );
            }
        }
    }
}
