﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UrsWorker.Parser.Abstractions;
using UrsWorker.Parser.Models;

namespace UrsWorker.Parser.Implementations
{
    /// <summary>
    /// Export URS modifications to Excel file
    /// </summary>
    public class ExcelUrsModificationsExporter : IUrsModificationsExporter
    {
        public Task ExportAsync(string exportFileName, IEnumerable<UrsModification> changes)
        {
            using (SpreadsheetDocument spreadsheet = SpreadsheetDocument.Create(exportFileName, SpreadsheetDocumentType.Workbook))
            {
                Export(spreadsheet, changes);
            }

            return Task.CompletedTask;
        }

        public Task ExportAsync(Stream stream, IEnumerable<UrsModification> changes)
        {
            using (SpreadsheetDocument spreadsheet = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook))
            {
                Export(spreadsheet, changes);
            }

            return Task.CompletedTask;
        }

        private void Export(SpreadsheetDocument spreadsheet, IEnumerable<UrsModification> changes)
        {
            InitSpreadsheet(spreadsheet);

            SheetData sheetData = spreadsheet.WorkbookPart.WorksheetParts.First().Worksheet
                .GetFirstChild<SheetData>();

            sheetData.Append(changes.Select((change, index) =>
            {
                Row row = new Row
                {
                    RowIndex = (uint)index + 1
                };

                row.Append(
                    CreateCell("A" + (index + 1), change.Title),
                    CreateCell("B" + (index + 1), change.Description)
                );

                return row;
            }));

            spreadsheet.WorkbookPart.WorkbookStylesPart.Stylesheet.Save();
            spreadsheet.WorkbookPart.Workbook.Save();
        }

        private void InitSpreadsheet(SpreadsheetDocument spreadsheet)
        {
            WorkbookPart workbookpart = spreadsheet.AddWorkbookPart();
            workbookpart.Workbook = new Workbook();

            WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
            worksheetPart.Worksheet = new Worksheet(new Columns(CreateColumn(1, 40), CreateColumn(2, 150)), new SheetData());

            Sheets sheets = spreadsheet.WorkbookPart.Workbook.AppendChild(new Sheets());

            Sheet sheet = new Sheet() { Id = spreadsheet.WorkbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet 1" };
            sheets.Append(sheet);

            WorkbookStylesPart stylesPart = workbookpart.AddNewPart<WorkbookStylesPart>();
            stylesPart.Stylesheet = new Stylesheet
            {
                
                Fills = new Fills(new Fill()),          // These properties  
                Fonts = new Fonts(new Font()),          // should be 
                Borders = new Borders(new Border()),    // initialized with any values
                CellFormats = new CellFormats(
                    new CellFormat(),     //First CellFormat should be empty
                    new CellFormat
                    {
                        ApplyAlignment = true,      // Enable
                        Alignment = new Alignment   // line wrapping
                        {
                            WrapText = true,
                            Vertical = new EnumValue<VerticalAlignmentValues>(VerticalAlignmentValues.Top)
                        },
                        ApplyFill = true
                    }
                )
            };
        }

        /// <param name="reference">Cell coordinate (A1)</param>
        /// <param name="data">Cell value</param>
        /// <returns></returns>
        private Cell CreateCell(string reference, string data) => new Cell
        {
            CellValue = new CellValue(data),
            CellReference = reference,
            DataType = new EnumValue<CellValues>(CellValues.String),
            StyleIndex = 1
        };

        /// <param name="index">Column index in table (starts from 1)</param>
        /// <param name="width">Column width</param>
        /// <returns></returns>
        private Column CreateColumn(uint index, double width) => new Column
        {
            BestFit = true,
            CustomWidth = true,
            Min = index,   
            Max = index,   
            Width = width
        };
    }
}
