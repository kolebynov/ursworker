﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UrsWorker.Parser.Models
{
    /// <summary>
    /// Paragraph nymber parsing.
    /// </summary>
    public class ParagraphNumber : IEquatable<ParagraphNumber>, IComparable<ParagraphNumber>
    {
        private const string Delimiter = ".";

        /// <summary>
        /// count of sections include subsections. (1.2.1 - 3 sections)
        /// </summary>
        public int Length => _parts.Count;

        public static readonly ParagraphNumber MinValue = $"0{Delimiter}0{Delimiter}0{Delimiter}0";
        public static readonly int MaxPartValue = 128;
        public static readonly int MaxParts = 4;
        public static readonly ParagraphNumber DefaultMaxValue = $"{MaxPartValue}{Delimiter}{MaxPartValue}{Delimiter}{MaxPartValue}{Delimiter}{MaxPartValue}";
        
        private List<int> _parts = new List<int>();
        private double Value
        {
            get {
                double res = 0;
                for (int i = 0; i < _parts.Count; i++)
                {
                    res += Math.Pow(MaxPartValue, MaxParts - i) * _parts[i];
                }
                return res;
            }
        }

        private string _number;
        public string Number {
            get { return _number; }
            set {
                _number = value;
                string[] parts = value.Split(new[] { Delimiter }, StringSplitOptions.RemoveEmptyEntries);
                _parts = parts.Select(x => int.Parse(x)).ToList();
            }
        }

        public ParagraphNumber(string number)
        {
            Number = number;
        }

        public ParagraphNumber()
        { }

        /// <summary>
        /// From - Inclusive, to - Exclusive.
        /// </summary>
        /// <param name="from">Inclusive</param>
        /// <param name="to">Exclusive</param>
        /// <returns></returns>
        public bool Between(ParagraphNumber from, ParagraphNumber to) =>
            this >= from && this < to;

        /// <summary>
        /// Increment last part (example: 5.2.2 -> 5.2.3)
        /// </summary>
        /// <returns></returns>
        public ParagraphNumber Increment() =>
            new ParagraphNumber(AddToLastPart(1));

        /// <summary>
        /// Decrement last part (example: 5.2.2 -> 5.2.1)
        /// </summary>
        /// <returns></returns>
        public ParagraphNumber Decrement() =>
            new ParagraphNumber(AddToLastPart(-1));

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is ParagraphNumber number && Equals(number);
        }

        public override int GetHashCode() => Value.GetHashCode();

        public override string ToString()
        {
            return string.Join(Delimiter, _parts);
        }

        public bool Equals(ParagraphNumber other) => other != null && Value == other.Value;

        public int CompareTo(ParagraphNumber other) => Value.CompareTo(other.Value);

        public static bool operator <(ParagraphNumber paragraphNumber1, ParagraphNumber paragraphNumber2) =>
            paragraphNumber1.CompareTo(paragraphNumber2) < 0;

        public static bool operator >(ParagraphNumber paragraphNumber1, ParagraphNumber paragraphNumber2) =>
            paragraphNumber1.CompareTo(paragraphNumber2) > 0;

        public static bool operator ==(ParagraphNumber paragraphNumber1, ParagraphNumber paragraphNumber2) =>
            Equals(paragraphNumber1, paragraphNumber2);

        public static bool operator !=(ParagraphNumber paragraphNumber1, ParagraphNumber paragraphNumber2) => 
            !(paragraphNumber1 == paragraphNumber2);

        public static bool operator <=(ParagraphNumber paragraphNumber1, ParagraphNumber paragraphNumber2) =>
            paragraphNumber1.CompareTo(paragraphNumber2) <= 0;

        public static bool operator >=(ParagraphNumber paragraphNumber1, ParagraphNumber paragraphNumber2) => 
            paragraphNumber1.CompareTo(paragraphNumber2) >= 0;

        public static implicit operator ParagraphNumber(string value) => new ParagraphNumber(value);

        private ParagraphNumber(List<int> parts)
        {
            _parts = parts;
        }

        private List<int> AddToLastPart(int add)
        {
            if(_parts.Count > 0) { 
                var res = new List<int>(_parts);
                res[res.Count - 1] += add;
                return res;
            }
            return new List<int>();
        }
    }
}
