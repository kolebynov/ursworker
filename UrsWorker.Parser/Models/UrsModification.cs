﻿namespace UrsWorker.Parser.Models
{
    public class UrsModification
    {
        public string Title { get; }
        public string Description { get; }

        public UrsModification(string title, string description)
        {
            Title = title;
            Description = description;
        }
    }
}
