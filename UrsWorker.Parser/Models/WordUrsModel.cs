﻿using System;
using System.Collections.Generic;
using System.Threading;
using UrsWorker.Parser.Implementations;
using UrsWorker.UrsModel;
using UrsWorker.UrsModel.Sections;

namespace UrsWorker.Parser.Models
{
    /// <inheritdoc />
    internal class WordUrsModel : IUrsModel
    {
        public string Protocol => _protocolLazy.Value;
        public string Version => _versionLazy.Value;
        public string IrtVersion => _irtVersionLazy.Value;
        public DateTime Date => _dateLazy.Value;
        public bool IsRandomizedStudy => _isRandomizedStudyLazy.Value;
        public IReadOnlyList<VersionSummary> VersionSummaries => _versionSummariesLazy.Value;
        public IReadOnlyList<KeywordReplacement> KeywordReplacements => _keywordReplacementsLazy.Value;
        public IReadOnlyList<Drug> Drugs => _drugsLazy.Value;
        public IReadOnlyList<Treatment> Treatments => _treatmentsLazy.Value;
        public IReadOnlyList<string> UnblindedDataPoints => _unblindedDataPointsLazy.Value;
        public IReadOnlyList<VisitSchedule> VisitSchedules => _visitSchedulesLazy.Value;
        public IReadOnlyList<StudyParameter> StudyParameters => _studyParametersLazy.Value;
        public IReadOnlyList<Country> Countries => _countriesLazy.Value;
        public IReadOnlyList<DataIntegration> DataIntegrations => _dataIntegrationsLazy.Value;
        public IReadOnlyList<Section> Sections => _sectionsLazy.Value;

        private readonly Lazy<string> _protocolLazy;
        private readonly Lazy<string> _versionLazy;
        private readonly Lazy<string> _irtVersionLazy;
        private readonly Lazy<DateTime> _dateLazy;
        private readonly Lazy<bool> _isRandomizedStudyLazy;
        private readonly Lazy<IReadOnlyList<VersionSummary>> _versionSummariesLazy;
        private readonly Lazy<IReadOnlyList<KeywordReplacement>> _keywordReplacementsLazy;
        private readonly Lazy<IReadOnlyList<Drug>> _drugsLazy;
        private readonly Lazy<IReadOnlyList<Treatment>> _treatmentsLazy;
        private readonly Lazy<IReadOnlyList<string>> _unblindedDataPointsLazy;
        private readonly Lazy<IReadOnlyList<VisitSchedule>> _visitSchedulesLazy;
        private readonly Lazy<IReadOnlyList<StudyParameter>> _studyParametersLazy;
        private readonly Lazy<IReadOnlyList<Country>> _countriesLazy;
        private readonly Lazy<IReadOnlyList<DataIntegration>> _dataIntegrationsLazy;
        private readonly Lazy<IReadOnlyList<Section>> _sectionsLazy;

        public WordUrsModel(WordUrsParser parser)
        {
            if (parser == null)
            {
                throw new ArgumentNullException(nameof(parser));
            }

            _protocolLazy = new Lazy<string>(parser.ParseProtocol, LazyThreadSafetyMode.None);
            _versionLazy = new Lazy<string>(parser.ParseVersion, LazyThreadSafetyMode.None);
            _irtVersionLazy = new Lazy<string>(parser.ParseIrtVersion, LazyThreadSafetyMode.None);
            _dateLazy = new Lazy<DateTime>(parser.ParseDate, LazyThreadSafetyMode.None);
            _isRandomizedStudyLazy = new Lazy<bool>(parser.ParseIsRandomizedStudy, LazyThreadSafetyMode.None);
            _versionSummariesLazy = new Lazy<IReadOnlyList<VersionSummary>>(parser.ParseVersionSummaries, LazyThreadSafetyMode.None);
            _keywordReplacementsLazy = new Lazy<IReadOnlyList<KeywordReplacement>>(parser.ParseKeywordReplacements, LazyThreadSafetyMode.None);
            _drugsLazy = new Lazy<IReadOnlyList<Drug>>(parser.ParseDrugs, LazyThreadSafetyMode.None);
            _treatmentsLazy = new Lazy<IReadOnlyList<Treatment>>(parser.ParseTreatments, LazyThreadSafetyMode.None);
            _unblindedDataPointsLazy = new Lazy<IReadOnlyList<string>>(parser.ParseUnblindedDataPoints, LazyThreadSafetyMode.None);
            _visitSchedulesLazy = new Lazy<IReadOnlyList<VisitSchedule>>(parser.ParseVisitSchedules, LazyThreadSafetyMode.None);
            _studyParametersLazy = new Lazy<IReadOnlyList<StudyParameter>>(parser.ParseStudyParameters, LazyThreadSafetyMode.None);
            _countriesLazy = new Lazy<IReadOnlyList<Country>>(parser.ParseCountries, LazyThreadSafetyMode.None);
            _dataIntegrationsLazy = new Lazy<IReadOnlyList<DataIntegration>>(parser.ParseDataIntegrations, LazyThreadSafetyMode.None);
            _sectionsLazy = new Lazy<IReadOnlyList<Section>>(parser.ParseSections, LazyThreadSafetyMode.None);
        }
    }
}
