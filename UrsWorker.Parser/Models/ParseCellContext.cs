﻿using DocumentFormat.OpenXml.Wordprocessing;
using System.Reflection;

namespace UrsWorker.Parser.Models
{
    public class ParseCellContext
    {
        public PropertyInfo ModelProperty { get; }
        public string ColumnName { get; }
        public TableCell Cell { get; }
        public bool IsValueParsed { get; set; }
        public object ParsedValue { get; set; }

        public ParseCellContext(PropertyInfo modelProperty, string columnName, TableCell cell)
        {
            ModelProperty = modelProperty;
            ColumnName = columnName;
            Cell = cell;
        }
    }
}
