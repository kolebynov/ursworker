﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Mvc;
using UrsWorker.Assembla.Abstractions;
using UrsWorker.Assembla.Models;
using UrsWorker.AssemblaTiketCreater.Models;

namespace UrsWorker.AssemblaTiketCreater.Controllers
{
    public class AssemblaController : Controller
    {
        private readonly IAssemblaTicketsService _assemblaTicketsService;
        private readonly IAssemblaChangesExporter _assemblaChangesExporter;
        private readonly IMilestoneProvider _milestoneProvider;
        private readonly ISpaceProvider _spaceProvider;

        public AssemblaController(
            IAssemblaTicketsService assemblaTicketsService,
            IAssemblaChangesExporter assemblaChangesExporter,
            IMilestoneProvider milestoneProvider,
            ISpaceProvider spaceProvider
        )
        {
            _assemblaTicketsService = assemblaTicketsService ?? throw new ArgumentNullException(nameof(assemblaTicketsService));
            _assemblaChangesExporter = assemblaChangesExporter ?? throw new ArgumentNullException(nameof(assemblaChangesExporter));
            _milestoneProvider = milestoneProvider ?? throw new ArgumentNullException(nameof(milestoneProvider));
            _spaceProvider = spaceProvider ?? throw new ArgumentNullException(nameof(spaceProvider));
        }

        [ResponseCache(Duration = 1800)]
        [HttpGet]
        public async Task<List<MilestoneModel>> GetMilestones(string spaceIdOrWikiNameFilter)
        {
            return await _milestoneProvider.FindAll(spaceIdOrWikiNameFilter);
        }

        [ResponseCache(Duration = 86400)]
        [HttpGet]
        public async Task<List<SpaceModel>> GetSpaces()
        {
            return await _spaceProvider.FindAll();
        }

        [HttpPost]
        public async Task<IActionResult> UpdateTicketsProtocol([FromForm] UpdateTicketsProtocolModel updateTicketsProtocolModel)
        {
            if (string.IsNullOrEmpty(updateTicketsProtocolModel.Protocol))
            {
                return Content("Protocol does not provided");
            }

            if (string.IsNullOrEmpty(updateTicketsProtocolModel.Space))
            {
                return Content("Space not selected");
            }

            if (string.IsNullOrEmpty(updateTicketsProtocolModel.Milestone))
            {
                return Content("Milestone not selected");
            }

            await _assemblaTicketsService.UpdateProtocolAsync(
                updateTicketsProtocolModel.Protocol,
                updateTicketsProtocolModel.Space,
                updateTicketsProtocolModel.Milestone
            );


            return Ok("The tickets protocol successfully updated");
        }

        [HttpPost]
        public async Task<IActionResult> CreateTickets([FromForm] CreateTicketsModel createTicketsModel)
        {
            if (createTicketsModel.FileWithChanges == null || createTicketsModel.FileWithChanges.Length == 0)
            {
                return Content("File not selected");
            }

            if (string.IsNullOrEmpty(createTicketsModel.Space))
            {
                return Content("Space not selected");
            }

            if (string.IsNullOrEmpty(createTicketsModel.Milestone))
            {
                return Content("Milestone not selected");
            }

            using (SpreadsheetDocument spreadsheetDocument =
                SpreadsheetDocument.Open(createTicketsModel.FileWithChanges.OpenReadStream(), false))
            {
                WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();

                var allRows = sheetData.Elements<Row>().ToList();
                Row firstRow = allRows[0];

                string protocol = string.Empty;
                var firstRowCells = firstRow.Elements<Cell>().ToList();
                if (GetCellValue(spreadsheetDocument, firstRowCells[0]) == "Protocol")
                {
                    protocol = GetCellValue(spreadsheetDocument, firstRowCells[1]);
                    allRows.Remove(firstRow);
                }

                //if (string.IsNullOrEmpty(protocol))
                //{
                //    throw new Exception("Protocol is not defined");
                //}

                List<AssemblaChange> changes = new List<AssemblaChange>();
                string ticketTitle;
                string ticketDescription;
                foreach (Row row in allRows)
                {
                    var rowCells = row.Elements<Cell>().ToList();
                    if (rowCells.Count == 0)
                    {
                        continue;
                    }

                    ticketTitle = GetCellValue(spreadsheetDocument, rowCells[0]);
                    if (string.IsNullOrEmpty(ticketTitle))
                    {
                        continue;
                    }
                    ticketDescription = (rowCells.Count >= 2) ? GetCellValue(spreadsheetDocument, rowCells[1]) : string.Empty;
                    changes.Add(new AssemblaChange()
                    {
                        Description = ticketDescription,
                        Title = ticketTitle,
                        Protocol = protocol
                    });
                }

                await _assemblaChangesExporter.UploadAsync(changes.Select(x => new AssemblaChange
                {
                    Title = x.Title,
                    Description = x.Description,
                    Protocol = x.Protocol
                }), createTicketsModel.Space, createTicketsModel.Milestone);

                return Ok("The tickets successfully created");
            }
        }

        private static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            if (cell.CellValue == null)
            {
                return null;
            }

            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerXml;

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }
    }
}