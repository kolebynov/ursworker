﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UrsWorker.AssemblaTiketCreater.Models;
using UrsWorker.Parser.Abstractions;
using UrsWorker.UrsModel;
using UrsWorker.Parser.Extensions;
using UrsWorker.Parser.Models;
using Microsoft.AspNetCore.Http;

namespace UrsWorker.AssemblaTiketCreater.Controllers
{
    public class HomeController : Controller
    {
        private readonly MainOptions _options;
        private readonly IUrsModelProvider _ursParser;
        private readonly IUrsModificationProvider _changesProvider;
        private readonly IUrsModificationsExporter _changesExporter;


        public HomeController(
            IUrsModelProvider ursParser,
            IUrsModificationProvider changesProvider,
            IUrsModificationsExporter changesExporter
        )
        {
            _ursParser = ursParser ?? throw new ArgumentNullException(nameof(ursParser));
            _changesProvider = changesProvider ?? throw new ArgumentNullException(nameof(changesProvider));
            _changesExporter = changesExporter ?? throw new ArgumentNullException(nameof(changesExporter));
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult TicketsProcessing()
        {
            return View();
        }

        [DisableRequestSizeLimit]
        [HttpPost]
        public async Task<IActionResult> UploadUrsAndDownloadChanges(IFormFile file)
        {
            if(file == null || file.Length == 0) {
                return Content("File not selected");
            }

            var resultStream = new MemoryStream();

            // process uploaded files
            IUrsModel ursModel = await _ursParser.GetAsync(file.OpenReadStream());
            List<UrsModification> changes =  _changesProvider.GetModifications(ursModel, true, true);

            await _changesExporter.ExportAsync(resultStream, changes);
            resultStream.Position = 0;

            string ursName = file.FileName.Replace(".docx", "");
            return File(resultStream, "application/octet-stream", $"{ursName}Changes_{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}.xlsx" );
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
