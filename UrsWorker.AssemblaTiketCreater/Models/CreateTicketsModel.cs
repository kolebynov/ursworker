﻿using Microsoft.AspNetCore.Http;

namespace UrsWorker.AssemblaTiketCreater.Models
{
    public class CreateTicketsModel
    {
        public IFormFile FileWithChanges { get; set; }
        public string Milestone { get; set; }
        public string Space { get; set; }
    }
}
