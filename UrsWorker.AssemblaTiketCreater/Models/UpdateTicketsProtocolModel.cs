﻿namespace UrsWorker.AssemblaTiketCreater.Models
{
    public class UpdateTicketsProtocolModel
    {
        public string Protocol { get; set; }
        public string Milestone { get; set; }
        public string Space { get; set; }
    }
}
