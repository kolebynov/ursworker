﻿using UrsWorker.Parser.Models;
using Xunit;

namespace UrsWorker.Parser.Tests.Models
{
    public class ParagraphNumberTests
    {
        [Theory]
        [InlineData("1", "2", -1)]
        [InlineData("1", "1.1", -1)]
        [InlineData("1.1", "1.2", -1)]
        [InlineData("1.1", "1.1.2", -1)]
        [InlineData("1.1.1", "1.1.2", -1)]
        [InlineData("1.1.1", "1.1.1.2", -1)]
        [InlineData("1.1.1.1", "1.1.1.2", -1)]
        [InlineData("1", "1", 0)]
        [InlineData("1.2", "1.2", 0)]
        [InlineData("1.2.3", "1.2.3", 0)]
        [InlineData("1.2.3.4", "1.2.3.4", 0)]
        [InlineData("2", "1", 1)]
        [InlineData("1.1", "1", 1)]
        [InlineData("1.2", "1.1", 1)]
        [InlineData("1.1.2", "1.1", 1)]
        [InlineData("1.1.2", "1.1.1", 1)]
        [InlineData("1.1.1.2", "1.1.1", 1)]
        [InlineData("1.1.1.2", "1.1.1.1", 1)]
        public void Can_Compare_Right(string number1, string number2, int expected)
        {
            //Arrange
            ParagraphNumber paragraphNumber1 = new ParagraphNumber(number1);
            ParagraphNumber paragraphNumber2 = new ParagraphNumber(number2);

            //Act
            bool isEqualFunc = paragraphNumber1.Equals(paragraphNumber2);
            int compareFunc = paragraphNumber1.CompareTo(paragraphNumber2);
            bool isEqualOperator = paragraphNumber1 == paragraphNumber2;
            bool isNotEqualOperator = paragraphNumber1 != paragraphNumber2;   
            bool num1LessNum2 = paragraphNumber1 < paragraphNumber2;
            bool num1LessOrEqualNum2 = paragraphNumber1 <= paragraphNumber2;
            bool num1GreaterNum2 = paragraphNumber1 > paragraphNumber2;
            bool num1GreaterOrEqualNum2 = paragraphNumber1 >= paragraphNumber2;

            //Assert
            Assert.Equal(expected == 0, isEqualFunc && isEqualOperator);
            Assert.Equal(expected != 0, isNotEqualOperator);
            Assert.Equal(expected < 0, compareFunc < 0);
            Assert.Equal(expected == 0, compareFunc == 0);
            Assert.Equal(expected > 0, compareFunc > 0);
            Assert.Equal(expected < 0, num1LessNum2 && num1LessOrEqualNum2);
            Assert.Equal(expected == 0, num1LessOrEqualNum2 && num1GreaterOrEqualNum2);
            Assert.Equal(expected > 0, num1GreaterNum2 && num1GreaterOrEqualNum2);
        }

        [Theory]
        [InlineData("1", 1)]
        [InlineData("1.2", 2)]
        [InlineData("1.2.3", 3)]
        [InlineData("1.2.3.4", 4)]
        [InlineData("0.1", 2)]
        [InlineData("0.1.2", 3)]
        [InlineData("0.1.2.3", 4)]
        [InlineData("1.0.1", 3)]
        [InlineData("1.0.0.1", 4)]
        public void Can_Return_Right_Length(string number, int expectedLength)
        {
            //Arrange
            ParagraphNumber paragraphNumber = number;

            //Act
            int actualLength = paragraphNumber.Length;

            //Asser
            Assert.Equal(expectedLength, actualLength);
        }

        [Theory]
        [InlineData("1", true, "2")]
        [InlineData("1.1", true, "1.2")]
        [InlineData("1.2.3", true, "1.2.4")]
        [InlineData("1.2.3.4", true, "1.2.3.5")]
        [InlineData("0.2.3.4", true, "0.2.3.5")]
        [InlineData("1.0.2", true, "1.0.3")]
        [InlineData("1.0.2", false, "1.0.1")]
        [InlineData("2", false, "1")]
        [InlineData("2.2", false, "2.1")]
        [InlineData("2.2.3", false, "2.2.2")]
        [InlineData("2.2.3.4", false, "2.2.3.3")]
        public void Can_Increment_Decrement(string number, bool inc, string expectedNumber)
        {
            //Arrange
            ParagraphNumber paragraphNumber = number;
            ParagraphNumber exptectedParagraphNumber = expectedNumber;

            //Act
            ParagraphNumber actualParagraphNumber = inc ? paragraphNumber.Increment() : paragraphNumber.Decrement();

            //Assert
            Assert.Equal(exptectedParagraphNumber, actualParagraphNumber);
        }

        [Theory]
        [InlineData("5.3.1", "5.3", "5.4", true)]
        [InlineData("5.3.127.127", "5.3", "5.4", true)]
        [InlineData("5.4", "5.3", "5.5", true)]
        [InlineData("5.4.127", "5.3", "5.5", true)]
        [InlineData("5.4", "5.3", "5.4", false)]
        public void Between(string number, string from, string to, bool isBetween)
        {
            ParagraphNumber paragraphNumber = number;
            ParagraphNumber fromParagraph = from;
            ParagraphNumber toParagraph = to;

            bool result = paragraphNumber.Between(fromParagraph, toParagraph);

            Assert.Equal(isBetween, result);
        }
    }
}
