﻿using System;

namespace UrsWorker.Configs.Extensions
{
    public static class StringExtensions
    {
        public static string RemoveBetween(this string str, string first, string second)
        {
            if (string.IsNullOrEmpty(str))
            {
                throw new ArgumentNullException(nameof(str));
            }

            int firstIndex = str.IndexOf(first, StringComparison.Ordinal);
            int secondIndex = str.LastIndexOf(second, StringComparison.Ordinal);
            if (firstIndex < 0 || secondIndex < 0 || firstIndex > secondIndex)
            {
                return str;
            }

            return str.Remove(firstIndex, secondIndex - firstIndex);
        }
    }
}
