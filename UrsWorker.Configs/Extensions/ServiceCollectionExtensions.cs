﻿using CsvHelper;
using Microsoft.Extensions.DependencyInjection;
using UrsWorker.Configs.Abstractions;
using UrsWorker.Configs.Implementations;

namespace UrsWorker.Configs.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddConfigsExport(this IServiceCollection services)
        {
            services.AddScoped<IConfigsProvider, ConfigsProvider>();
            services.AddScoped<IVisitHelper, VisitHelper>();
            services.AddSingleton<IFactory, Factory>();
            services.AddSingleton<IConfigWriterFactory, ConfigWriterFactory>();         

            return services;
        }
    }
}
