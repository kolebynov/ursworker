﻿using CsvHelper.Configuration.Attributes;

namespace UrsWorker.Configs.Models
{
    public class PatientTreatment
    {
        [Name("TreatmentId")]
        public string Id { get; set; }

        [Name("ClientDescription")]
        public string Description { get; set; }

        [Name("IWRDescription")]
        public string IwrDescription => Description;

        [Name("IVRDescription")]
        public string IvrDescription => Description;
    }
}
