﻿using CsvHelper.Configuration.Attributes;

namespace UrsWorker.Configs.Models
{
    public class VisitSchedule
    {
        [Name("VisitScheduleId")]
        public string Id { get; set; }

        [Name("VisitScheduleName")]
        public string Name { get; set; }

        [Name("SortOrderIndex")]
        public int SortOrder { get; set; }
    }
}
