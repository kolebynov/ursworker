﻿using CsvHelper.Configuration.Attributes;

namespace UrsWorker.Configs.Models
{
    public class Visit
    {
        [Name("VisitID")]
        public string Id { get; set; }

        [Name("VisitName")]
        public string Name { get; set; }

        [Name("Index")]
        public int Index { get; set; }

        [Name("IsUnscheduled")]
        public bool IsUnscheduled { get; set; }

        [Name("RuntimeGeneratedFromVisitId")]
        public string RuntimeGeneratedFromVisitId { get; set; }
    }
}
