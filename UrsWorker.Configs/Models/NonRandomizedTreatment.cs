﻿using CsvHelper.Configuration.Attributes;

namespace UrsWorker.Configs.Models
{
    public class NonRandomizedTreatment : PatientTreatment
    {
        [Name("Visit Id")]
        public string VisitId { get; set; }
    }
}
