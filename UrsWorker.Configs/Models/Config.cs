﻿namespace UrsWorker.Configs.Models
{
    public class Config
    {
        public string FileName { get; }
        public string Data { get; }

        public Config(string fileName, string data)
        {
            FileName = fileName;
            Data = data;
        }
    }
}
