﻿using System.IO;

namespace UrsWorker.Configs.Abstractions
{
    public interface IConfigWriterFactory
    {
        IConfigWriter<T> Create<T>(TextWriter writer);
    }
}
