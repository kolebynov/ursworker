﻿using UrsWorker.Configs.Models;
using UrsWorker.UrsModel;

namespace UrsWorker.Configs.Abstractions
{
    /// <summary>
    /// Возвращает различные конфиги
    /// </summary>
    public interface IConfigsProvider
    {
        Config GetCountries(IUrsModel model);
        Config GetDrugs(IUrsModel model);
        Config GetTreatments(IUrsModel model);
        Config GetVisitSchedules(IUrsModel model);
        Config GetVisits(IUrsModel model);
    }
}
