﻿using UrsWorker.UrsModel;

namespace UrsWorker.Configs.Abstractions
{
    public interface IVisitHelper
    {
        string GetVisitScheduleId(VisitSchedule visitSchedule);
        string GetVisitId(Visit visit);
        Visit GetRandomizationVisit(VisitSchedule visitSchedule);
    }
}
