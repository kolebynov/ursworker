﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace UrsWorker.Configs.Abstractions
{
    /// <summary>
    /// Пишет конфиги.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IConfigWriter<in T> : IDisposable
    {
        /// <summary>
        /// Расширение, в формате которого производится запись
        /// </summary>
        string Extension { get; }

        Task WriteAsync(IEnumerable<T> records);
    }
}
