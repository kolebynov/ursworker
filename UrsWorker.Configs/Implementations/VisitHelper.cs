﻿using System;
using System.Linq;
using System.Text;
using UrsWorker.Configs.Abstractions;
using UrsWorker.Configs.Extensions;
using UrsWorker.UrsModel;

namespace UrsWorker.Configs.Implementations
{
    public class VisitHelper : IVisitHelper
    {
        private const string VisitSchedule = "Visit Schedule";  
        private const string ScreeningVisitId = "SCR";
        private const string ScreeningVisitName = "Screening";
        private const string RandomizationVisitId = "RAND";

        private static readonly string[] RandomizationVisitMarks = {"Enrollment", "Baseline", "Randomization"};

        public string GetVisitScheduleId(VisitSchedule visitSchedule) =>
            visitSchedule.Name
                .Replace(VisitSchedule, "")
                .Replace(" ", "");

        public string GetVisitId(Visit visit)
        {
            if (IsScreeningVisit(visit))
            {
                return ScreeningVisitId;
            }

            if (IsRandomizationVisit(visit))
            {
                return RandomizationVisitId;
            }

            string[] parts = visit.Name.RemoveBetween("(", ")").Split(new []{' '}, StringSplitOptions.RemoveEmptyEntries);

            return parts.Aggregate(new StringBuilder(10), (sb, part) =>
            {
                if (char.IsUpper(part[0]))
                {
                    sb.Append(part[0]);
                }
                else if (char.IsDigit(part[0]))
                {
                    sb.Append(part);
                }

                return sb;
            }, sb => sb.ToString());
        }

        public Visit GetRandomizationVisit(VisitSchedule visitSchedule) =>
            visitSchedule.Visits.FirstOrDefault(IsRandomizationVisit);

        private bool IsRandomizationVisit(Visit visit) =>
            RandomizationVisitMarks.Any(mark => visit.Name.Contains(mark));

        private bool IsScreeningVisit(Visit visit) => visit.Name == ScreeningVisitName;
    }
}
