﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using UrsWorker.Configs.Abstractions;
using UrsWorker.Configs.CsvMaps;
using UrsWorker.UrsModel;

namespace UrsWorker.Configs.Implementations
{
    public class ConfigWriterFactory : IConfigWriterFactory
    {
        private readonly IFactory _csvFactory;

        private static readonly Dictionary<Type, ClassMap> CsvClassMaps = new Dictionary<Type, ClassMap>
        {
            [typeof(Country)] = new CountryMap(),
            [typeof(Drug)] = new DrugMap()
        };

        public ConfigWriterFactory(IFactory csvFactory)
        {
            _csvFactory = csvFactory ?? throw new ArgumentNullException(nameof(csvFactory));
        }

        public IConfigWriter<T> Create<T>(TextWriter writer)
        {
            IWriter csvWriter = _csvFactory.CreateWriter(writer);

            if (CsvClassMaps.TryGetValue(typeof(T), out ClassMap classMap))
            {
                csvWriter.Configuration.RegisterClassMap(classMap);
            }

            return new ConfigWriter<T>(csvWriter);
        }
    }
}
