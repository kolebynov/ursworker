﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UrsWorker.Configs.Abstractions;

namespace UrsWorker.Configs.Implementations
{
    /// <inheritdoc />
    internal class ConfigWriter<T> : IConfigWriter<T>
    {
        private readonly IWriter _writer;

        /// <inheritdoc />
        public string Extension => "csv";

        public ConfigWriter(IWriter writer)
        {
            _writer = writer ?? throw new ArgumentNullException(nameof(writer));
        }

        public Task WriteAsync(IEnumerable<T> records)
        {
            _writer.WriteRecords(records);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _writer?.Dispose();
        }
    }
}
