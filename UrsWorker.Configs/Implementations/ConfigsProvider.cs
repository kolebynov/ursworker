﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UrsWorker.Configs.Abstractions;
using UrsWorker.Configs.Models;
using UrsWorker.UrsModel;
using Visit = UrsWorker.UrsModel.Visit;

namespace UrsWorker.Configs.Implementations
{
    public class ConfigsProvider : IConfigsProvider
    {
        private const string CountriesFileName = "Countries";
        private const string DrugsFileName = "DrugCodes";
        private const string RandomizedTreatmentsFileName = "PatientTreatments";
        private const string NonRandomizedTreatmentsFileName = "NonRandomizedTreatments";
        private const string VisitSchedulesFileName = "VisitSchedules";
        private const string VisitsFileName = "Visits";

        private readonly IConfigWriterFactory _configWriterFactory;
        private readonly IVisitHelper _visitHelper;

        public ConfigsProvider(IConfigWriterFactory configWriterFactory, IVisitHelper visitHelper)
        {
            _configWriterFactory = configWriterFactory ?? throw new ArgumentNullException(nameof(configWriterFactory));
            _visitHelper = visitHelper ?? throw new ArgumentNullException(nameof(visitHelper));
        }

        public Config GetCountries(IUrsModel model) =>
            GetConfig(model.Countries, CountriesFileName);

        public Config GetDrugs(IUrsModel model) =>
            GetConfig(model.Drugs, DrugsFileName);

        public Config GetTreatments(IUrsModel model)
        {
            if (model.IsRandomizedStudy)
            {
                return GetConfig(UrsTreatmentToConfigTreatment<PatientTreatment>(model.Treatments),
                    RandomizedTreatmentsFileName);
            }

            string randVisitId = _visitHelper.GetVisitId(_visitHelper.GetRandomizationVisit(model.VisitSchedules[0]));
            return GetConfig(
                UrsTreatmentToConfigTreatment<NonRandomizedTreatment>(model.Treatments, (t, _) => t.VisitId = randVisitId), 
                NonRandomizedTreatmentsFileName);
        }

        public Config GetVisitSchedules(IUrsModel model) =>
            GetConfig(model.VisitSchedules.Select((v, index) => new Models.VisitSchedule
            {
                Id = _visitHelper.GetVisitScheduleId(v),
                Name = v.Name,
                SortOrder = index + 1
            }), VisitSchedulesFileName);

        public Config GetVisits(IUrsModel model) =>
            GetConfig(model.VisitSchedules
                    .SelectMany(x => x.Visits)
                    .Distinct(new VisitNamesComparer())
                    .Select((x, index) => new Models.Visit
                    {
                        Id = _visitHelper.GetVisitId(x),
                        Name = x.Name,
                        Index = index + 1,
                        IsUnscheduled = false
                    }),
                VisitsFileName);


        private Config GetConfig<T>(IEnumerable<T> items, string fileName)
        {
            using (StringWriter sw = new StringWriter())
            {
                using (IConfigWriter<T> writer = _configWriterFactory.Create<T>(sw))
                {
                    writer.WriteAsync(items).Wait();
                    return new Config(Path.ChangeExtension(fileName, writer.Extension), sw.ToString());
                }
            }
        }

        private IEnumerable<T> UrsTreatmentToConfigTreatment<T>(IEnumerable<Treatment> treatments,
            Action<T, Treatment> addAction = null)
            where T : PatientTreatment, new() =>
            treatments.Select(t =>
            {
                T treatment = new T
                {
                    Id = t.Code,
                    Description = t.Description
                };

                addAction?.Invoke(treatment, t);
                return treatment;
            });

        private class VisitNamesComparer : IEqualityComparer<Visit>
        {
            public bool Equals(Visit x, Visit y) =>
                string.Equals(x?.Name, y?.Name, StringComparison.OrdinalIgnoreCase);

            public int GetHashCode(Visit obj) => obj.Name.GetHashCode();
        }
    }
}
