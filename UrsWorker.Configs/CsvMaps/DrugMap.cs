﻿using CsvHelper.Configuration;
using UrsWorker.UrsModel;

namespace UrsWorker.Configs.CsvMaps
{
    internal class DrugMap : ClassMap<Drug>
    {
        public DrugMap()
        {
            Map(x => x.Code).Name("DrugCode");
            Map(x => x.UnblindedDescription).Name("Unblinded Drug Description");
            Map(x => x.SiteUnitMultiplier).Name("Site Dispensing Unit Multiplier");
            Map(x => x.BlindingGroup).Name("Blinding Group");
            Map(x => x.ResupplyGroup).Name("Resupply Group");
            Map(x => x.ShippingGroup).Name("Shipping Group");
            Map(x => x.MaterialType).Name("Material Type");
            Map(x => x.BlindedDescription).Name("Blinded Drug Description");
            Map(x => x.PackagingGroup).Name("Packaging Group");
        }
    }
}
