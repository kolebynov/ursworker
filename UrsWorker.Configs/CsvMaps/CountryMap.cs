﻿using CsvHelper.Configuration;
using UrsWorker.UrsModel;

namespace UrsWorker.Configs.CsvMaps
{
    internal class CountryMap : ClassMap<Country>
    {
        public CountryMap()
        {
            Map(c => c.Name).Name("Country");
            Map(c => c.Code).Name("CountryCode");
            Map(c => c.Language).Name("Language");
        }
    }
}
