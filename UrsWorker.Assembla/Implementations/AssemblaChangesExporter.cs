﻿using Kralizek.Assembla;
using Kralizek.Assembla.Connector.Tickets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UrsWorker.Assembla.Abstractions;
using UrsWorker.Assembla.Models;

namespace UrsWorker.Assembla.Implementations
{
    public class AssemblaChangesExporter : IAssemblaChangesExporter
    {
        private const string TicketType = "Task";
        private const string Environment = "Dev";

        private readonly IAssemblaClient _assemblaClient;

        public AssemblaChangesExporter(IAssemblaClient assemblaClient)
        {
            _assemblaClient = assemblaClient ?? throw new ArgumentNullException(nameof(assemblaClient));
        }

        public async Task UploadAsync(IEnumerable<AssemblaChange> changes, string spaceId, string milestoneId)
        {
            var milestone = await _assemblaClient.Milestones.GetAsync(spaceId, milestoneId);
            if(milestone == null)
            {
                throw new InvalidOperationException("Milestone not found");
            }
            
            await Task.WhenAll(changes.Select(change => _assemblaClient.Tickets.CreateAsync(spaceId,
                new Ticket
                {
                    MilestoneId = milestoneId,
                    Description = change.Description,
                    Status = "New",
                    State = TicketState.Open,
                    Summary = change.Title,
                    CustomFields = new Dictionary<string, string>
                    {
                        ["Protocol"] = change.Protocol,
                        ["Ticket Type"] = TicketType,
                        ["Environment"] = Environment
                    }
                })));
        }
    }
}
