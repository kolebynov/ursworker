﻿using Kralizek.Assembla;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using UrsWorker.Assembla.Abstractions;
using UrsWorker.Assembla.Models;

namespace UrsWorker.Assembla.Implementations
{
    public class AssemblaSpaceProvider : ISpaceProvider
    {
        private readonly IAssemblaClient _assemblaClient;

        public AssemblaSpaceProvider(IAssemblaClient assemblaClient)
        {
            _assemblaClient = assemblaClient ?? throw new ArgumentNullException(nameof(assemblaClient));
        }

        public async Task<List<SpaceModel>> FindAll()
        {
            var milestones = await _assemblaClient.Spaces.GetAllAsync();
            return milestones
                .Select(m => new SpaceModel()
                {
                    Id = m.Id,
                    Name = m.Name
                }).ToList();
        }
    }
}
