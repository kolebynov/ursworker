﻿using Kralizek.Assembla;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using UrsWorker.Assembla.Abstractions;
using UrsWorker.Assembla.Models;
using Kralizek.Assembla.Connector.Milestones;

namespace UrsWorker.Assembla.Implementations
{
    public class AssemblaMilestoneProvider: IMilestoneProvider
    {
        private readonly IAssemblaClient _assemblaClient;

        public AssemblaMilestoneProvider(IAssemblaClient assemblaClient)
        {
            _assemblaClient = assemblaClient ?? throw new ArgumentNullException(nameof(assemblaClient));
        }

        public async Task<List<MilestoneModel>> FindAll(string spaceIdOrWikiName)
        {
            int page = 1;
            int pageSize = 100;
            List<MilestoneModel> result = new List<MilestoneModel>();
            IReadOnlyList<Milestone> milestones;

            do
            {
                milestones = await _assemblaClient.Milestones.GetAllAsync(spaceIdOrWikiName, page, pageSize);
                result.AddRange(
                    milestones.Select(m => new MilestoneModel()
                    {
                        Id = m.Id,
                        Name = m.Title
                    })
                );
                page++;
            } while (milestones.Count == pageSize);

            return result.OrderBy(x => x.Name.ToLower()).ToList();
        }

        public async Task<MilestoneModel> Find(string milestoneId)
        {
            var milestone = await _assemblaClient.Milestones.GetAsync(spaceIdOrWikiName: "", milestoneId: milestoneId);
            return new MilestoneModel()
            {
                Id = milestone.Id,
                Name = milestone.Title
            };
        }
    }
}
