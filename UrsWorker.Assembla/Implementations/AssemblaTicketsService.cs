﻿using Kralizek.Assembla;
using Kralizek.Assembla.Connector.Tickets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UrsWorker.Assembla.Abstractions;
using UrsWorker.Assembla.Models;

namespace UrsWorker.Assembla.Implementations
{
    public class AssemblaTicketsService : IAssemblaTicketsService
    {
        private readonly IAssemblaClient _assemblaClient;

        public AssemblaTicketsService(IAssemblaClient assemblaClient)
        {
            _assemblaClient = assemblaClient ?? throw new ArgumentNullException(nameof(assemblaClient));
        }

        public async Task UpdateProtocolAsync(string protocol, string spaceId, string milestoneId)
        {
            if (string.IsNullOrEmpty(protocol))
            {
                throw new ArgumentNullException(nameof(protocol));
            }

            var milestone = await _assemblaClient.Milestones.GetAsync(spaceId, milestoneId);
            if(milestone == null)
            {
                throw new InvalidOperationException("Milestone not found");
            }

            string protocolKey = "Protocol";
            int page = 1;
            int pageSize = 100;
            List<Ticket> result = new List<Ticket>();
            IReadOnlyList<Ticket> tickets;

            do
            {
                tickets = await _assemblaClient.Tickets.GetInMilestoneAsync(spaceId, milestoneId, page, pageSize);
                foreach(Ticket ticket in tickets)
                {
                    if (ticket.CustomFields == null)
                    {
                        ticket.CustomFields = new Dictionary<string, string>
                        {
                            [protocolKey] = protocol
                        };
                    }
                    else
                    {
                        if (ticket.CustomFields.ContainsKey(protocolKey))
                        {
                            ticket.CustomFields[protocolKey] = protocol;
                        }
                        else
                        {
                            ticket.CustomFields.Add(protocolKey, protocol);
                        }
                    }

                    await _assemblaClient.Tickets.UpdateAsync(spaceId, ticket);
                }

                page++;
            } while (tickets.Count == pageSize);
        }
    }
}
