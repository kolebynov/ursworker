﻿using Kralizek.Assembla;
using Kralizek.Assembla.Connector;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using UrsWorker.Assembla.Abstractions;
using UrsWorker.Assembla.Implementations;
using UrsWorker.Assembla.Models;

namespace UrsWorker.Assembla.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddAssembla(this IServiceCollection services, Action<AssemblaOptions> config = null)
        {
            services.AddScoped<AssemblaAuthenticator, UserAuthenticator>();
            services.AddScoped<IAssemblaClient, HttpAssemblaClient>();
            services.AddScoped<IAssemblaChangesExporter, AssemblaChangesExporter>();
            services.AddScoped<IMilestoneProvider, AssemblaMilestoneProvider>();
            services.AddScoped<ISpaceProvider, AssemblaSpaceProvider>();
            services.AddScoped<IAssemblaTicketsService, AssemblaTicketsService>();
            services.Configure<AssemblaOptions>(options => config?.Invoke(options));
            services.AddSingleton(sp =>
            {
                AssemblaOptions options = sp.GetRequiredService<IOptions<AssemblaOptions>>().Value;
                return Options.Create(new AssemblaAuthenticatorOptions
                {
                    ApiSecretKey = options.ApiSecretKey,
                    ApiKey = options.ApiKey,
                    ServiceUri = options.ServiceUri
                });
            });

            return services;
        }
    }
}
