﻿using System;

namespace UrsWorker.Assembla.Models
{
    public class AssemblaOptions
    {
        public string ApiKey { get; set; }
        public string ApiSecretKey { get; set; }
        public Uri ServiceUri { get; set; }
    }
}
