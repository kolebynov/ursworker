﻿namespace UrsWorker.Assembla.Models
{
    public class AssemblaChange
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Protocol { get; set; }
    }
}
