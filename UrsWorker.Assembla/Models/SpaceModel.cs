﻿namespace UrsWorker.Assembla.Models
{
    public class SpaceModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
