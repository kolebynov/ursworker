﻿namespace UrsWorker.Assembla.Models
{
    public class MilestoneModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
