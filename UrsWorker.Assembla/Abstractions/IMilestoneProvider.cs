﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UrsWorker.Assembla.Models;

namespace UrsWorker.Assembla.Abstractions
{
    public interface IMilestoneProvider
    {
        Task<List<MilestoneModel>> FindAll(string spaceIdOrWikiName);
        Task<MilestoneModel> Find(string milestoneId);
    }
}
