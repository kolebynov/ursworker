﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UrsWorker.Assembla.Models;

namespace UrsWorker.Assembla.Abstractions
{
    public interface IAssemblaChangesExporter
    {
        /// <param name="changes">List of changes</param>
        /// <param name="spaceId">Space id</param>
        /// <param name="milestoneId">Milestone id</param>
        /// <returns></returns>
        Task UploadAsync(IEnumerable<AssemblaChange> changes, string spaceId, string milestoneId);
    }
}
