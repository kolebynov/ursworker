﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UrsWorker.Assembla.Models;

namespace UrsWorker.Assembla.Abstractions
{
    public interface ISpaceProvider
    {
        Task<List<SpaceModel>> FindAll();
    }
}
