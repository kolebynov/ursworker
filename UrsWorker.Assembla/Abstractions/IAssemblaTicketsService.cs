﻿using System.Threading.Tasks;

namespace UrsWorker.Assembla.Abstractions
{
    public interface IAssemblaTicketsService
    {
        Task UpdateProtocolAsync(string protocol, string spaceId, string milestoneId);
    }
}
