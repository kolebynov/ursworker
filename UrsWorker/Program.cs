﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;
using UrsWorker.Assembla.Extensions;
using UrsWorker.Assembla.Models;
using UrsWorker.Configs.Extensions;
using UrsWorker.Parser.Extensions;
using UrsWorker.Parser.Models;
using UrsWorker.Parser.Settings;
using UrsWorker.Resources;

namespace UrsWorker
{
    class Program
    {
        static async Task Main(string[] args)
        {
            if (args.Length > 2)
            {
                Console.WriteLine(StringResources.TooFewArguments);
                Console.Read();
                return;
            }

            string documentFilePath = GetFromArgsOrConsole(args, 0, StringResources.DocumentPathInputPrompt);
            string exportFilePath = GetFromArgsOrDefault(args, 1, Path.ChangeExtension(documentFilePath, "xlsx"));
            bool uploadChanges = bool.Parse(GetFromArgsOrConsole(args, 999, "Upload changes?: "));

            var host = new HostBuilder()
                .ConfigureHostConfiguration(config =>
                {
                    config.AddCommandLine(args);
                })
                .ConfigureAppConfiguration(config =>
                {
                    config.SetBasePath(Directory.GetCurrentDirectory());
                    config.AddJsonFile("settings.json");
                })
                .ConfigureServices((hostBuilder, services) =>
                {
                    var configuration = hostBuilder.Configuration;
                    services.AddOptions();
                    services.Configure<ParserSettings>(configuration.GetSection("parserSettings"));
                    services.Configure<MainOptions>(options =>
                    {
                        options.DocumentFilePath = documentFilePath;
                        options.ExportFilePath = exportFilePath;
                        options.InitialChanges = new[]
                        {
                            new UrsModification("Initial configuration", ""),
                        };
                        options.UploadChanges = uploadChanges;
                        options.AssemblaSpace = "Suvoda Dev Training";
                        options.AssemblaMilestone = "MMalakhouski Training Development";
                    });

                    services.AddHostedService<Main>();
                    services.AddUrsParser();
                    services.AddAssembla();
                    services.Configure<AssemblaOptions>(configuration.GetSection("assemblaSettings"));
                    services.AddConfigsExport();
                })
                .ConfigureLogging(config =>
                {
                    config.AddConsole();
                    config.SetMinimumLevel(LogLevel.Information);
                })
                .UseConsoleLifetime()
                .Build();

            await host.RunAsync();
        }

        static string GetFromArgsOrConsole(string[] args, int index, string promptString)
        {
            if (args.Length > index)
            {
                return args[index];
            }

            Console.Write(promptString);
            return Console.ReadLine();
        }

        static string GetFromArgsOrDefault(string[] args, int index, string def = "") => args.Length > index ? args[index] : def;
    }
}
