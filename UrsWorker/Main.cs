﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UrsWorker.Assembla.Abstractions;
using UrsWorker.Assembla.Models;
using UrsWorker.Configs.Abstractions;
using UrsWorker.Configs.Models;
using UrsWorker.Parser;
using UrsWorker.Parser.Abstractions;
using UrsWorker.Parser.Extensions;
using UrsWorker.Parser.Models;
using UrsWorker.UrsModel;

namespace UrsWorker
{
    public class Main : IHostedService, IDisposable
    {
        private readonly ILogger<Main> _logger;
        private readonly IUrsModificationProvider _changesProvider;
        private readonly IUrsModificationsExporter _changesExporter;
        private readonly MainOptions _options;
        private readonly IUrsModelProvider _ursParser;
        private readonly IAssemblaChangesExporter _assemblaChangesExporter;
        private readonly IConfigsProvider _configsProvider;
        private CancellationTokenSource _cancellationTokenSource;
        private Task _processTask;

        public Main(
            ILogger<Main> logger, 
            IUrsModificationProvider changesProvider, 
            IOptions<MainOptions> options, 
            IUrsModificationsExporter changesExporter, 
            IUrsModelProvider ursParser, 
            IAssemblaChangesExporter assemblaChangesExporter, IConfigsProvider configsProvider)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _changesProvider = changesProvider ?? throw new ArgumentNullException(nameof(changesProvider));
            _changesExporter = changesExporter ?? throw new ArgumentNullException(nameof(changesExporter));
            _ursParser = ursParser ?? throw new ArgumentNullException(nameof(ursParser));
            _assemblaChangesExporter = assemblaChangesExporter ?? throw new ArgumentNullException(nameof(assemblaChangesExporter));
            _configsProvider = configsProvider ?? throw new ArgumentNullException(nameof(configsProvider));
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _processTask = Task.Run(async () => await ProccessDocumentAsync(_cancellationTokenSource.Token), _cancellationTokenSource.Token);
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _cancellationTokenSource.Cancel();
            return _processTask;
        }

        public void Dispose()
        {
            _cancellationTokenSource?.Dispose();
        }

        private async Task ProccessDocumentAsync(CancellationToken cancellationToken)
        {
            Stopwatch sw = new Stopwatch();
            try
            {
                _logger.LogInformation("Start");
                
                sw.Start();
                cancellationToken.ThrowIfCancellationRequested();

                IUrsModel ursModel = await _ursParser.ParseAsync(_options.DocumentFilePath);

                List<UrsModification> changes = new List<UrsModification>(60);
                changes.AddRange(_options.InitialChanges ?? new UrsModification[0]);
                changes.AddRange(_changesProvider.GetModifications(ursModel));

                _logger.LogInformation($"Successful parsing, time: {sw.Elapsed.TotalSeconds}s");

                await _changesExporter.ExportAsync(_options.ExportFilePath, changes);

                _logger.LogInformation($"Successful export to file, time: {sw.Elapsed.TotalSeconds}s");

                if (_options.UploadChanges)
                {
                    await _assemblaChangesExporter.UploadAsync(changes.Skip(7).Take(2).Select(x => new AssemblaChange
                    {
                        Title = x.Title,
                        Description = x.Description,
                        Protocol = ursModel.Protocol
                    }), _options.AssemblaSpace, _options.AssemblaMilestone);

                    _logger.LogInformation($"Successful upload to assembla, time: {sw.Elapsed.TotalSeconds}s");
                }

                await SaveConfigToFileAsync(_configsProvider.GetCountries(ursModel));
                await SaveConfigToFileAsync(_configsProvider.GetDrugs(ursModel));
                await SaveConfigToFileAsync(_configsProvider.GetTreatments(ursModel));
                await SaveConfigToFileAsync(_configsProvider.GetVisitSchedules(ursModel));
                await SaveConfigToFileAsync(_configsProvider.GetVisits(ursModel));

                _logger.LogInformation($"Done, time: {sw.Elapsed.TotalSeconds}s");
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
                throw new AggregateException(e);
            }
            finally
            {
                sw.Stop();
            }
        }

        private async Task SaveConfigToFileAsync(Config config)
        {
            using (FileStream fs = File.Create(
                Path.Combine(Path.GetDirectoryName(_options.ExportFilePath), config.FileName),
                Constants.DefaultFileBufferSize,
                FileOptions.Asynchronous
            ))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    await sw.WriteAsync(config.Data);
                }
            }
        }
    }
}
