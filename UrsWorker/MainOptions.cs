﻿using System.Collections.Generic;
using UrsWorker.Parser.Models;

namespace UrsWorker
{
    public class MainOptions
    {
        public string DocumentFilePath { get; set; }
        public string ExportFilePath { get; set; }
        public IEnumerable<UrsModification> InitialChanges { get; set; }
        public bool UploadChanges { get; set; }
        public string AssemblaSpace { get; set; }
        public string AssemblaMilestone { get; set; }
    }
}
