﻿using UrsWorker.UrsModel.Attributes;

namespace UrsWorker.UrsModel
{
    public class StudyParameter : IUrsItem
    {
        [NamesInUrs("Parameter Name")]
        public string Name { get; set; }

        [NamesInUrs("Description")]
        public string Description { get; set; }

        [NamesInUrs("Setting")]
        public string Value { get; set; }

        public override string ToString() => $"{Name} = {Value}";
    }
}
