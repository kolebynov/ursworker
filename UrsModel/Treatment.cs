﻿using UrsWorker.UrsModel.Attributes;

namespace UrsWorker.UrsModel
{
    public class Treatment : IUrsItem
    {
        [NamesInUrs("Treatment Code")]
        public string Code { get; set; }

        [NamesInUrs("Treatment Description")]
        public string Description { get; set; }

        public override string ToString() => $"{Code} - {Description}";
    }
}
