﻿using UrsWorker.UrsModel.Attributes;

namespace UrsWorker.UrsModel.Common
{
    public enum Blinding
    {
        [NamesInUrs("N/A")]
        None = 1,
        Blinded = 2,
        Unblinded = 3
    }
}
