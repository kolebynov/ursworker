﻿using UrsWorker.UrsModel.Attributes;

namespace UrsWorker.UrsModel.Common
{
    public enum MaterialType
    {
        Serialized = 1,
        [NamesInUrs("Non-Serialized")]
        NonSerialized = 2,
        Ancillary = 3
    }
}
