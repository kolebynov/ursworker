﻿using System.Collections.Generic;
using UrsWorker.UrsModel.Attributes;

namespace UrsWorker.UrsModel
{
    public class Country : IUrsItem
    {
        [NamesInUrs("Country Code")]
        public string Code { get; set; }

        [NamesInUrs("Country")]
        public string Name { get; set; }

        [NamesInUrs("Demographics Captured")]
        public string DemographicsCaptured { get; set; }

        [NamesInUrs("DOB Data Points")]
        public string DobDataPoints { get; set; }

        public string Language { get; set; }

        public override string ToString() => $"{Code} - {Name}";
    }
}
