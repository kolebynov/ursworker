﻿using System;

namespace UrsWorker.UrsModel.Attributes
{
    /// <summary>
    /// urs field desriptions mapping
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class NamesInUrsAttribute : Attribute
    {
        public string [] Names { get; }

        public NamesInUrsAttribute(params string [] names)
        {
            Names = names;
        }
    }
}
