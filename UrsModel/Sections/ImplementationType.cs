﻿using UrsWorker.UrsModel.Attributes;

namespace UrsWorker.UrsModel.Sections
{
    public enum ImplementationType
    {
        Standard = 1,
        [NamesInUrs("Not Utilized")]
        NotUtilized = 2,
        Modified = 3,
        New = 4
    }
}
