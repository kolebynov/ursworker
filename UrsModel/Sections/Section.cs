﻿using System.Collections.Generic;
using UrsWorker.UrsModel.Common;

namespace UrsWorker.UrsModel.Sections
{
    public class Section
    {
        public string Number { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// section implementation state
        /// </summary>
        public ImplementationType Implementation { get; set; }

        public Blinding Blinding { get; set; }

        /// <summary>
        /// section changes
        /// </summary>
        public IEnumerable<ChangeDescription> ChangeDescriptions { get; set; }

        public override string ToString() => $"{Number} {Name}. Implementation: {Implementation}. Blinding: {Blinding}";
    }
}
