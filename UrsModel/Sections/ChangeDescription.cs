﻿using System.Collections.Generic;
using UrsWorker.UrsModel.Attributes;

namespace UrsWorker.UrsModel.Sections
{
    public class ChangeDescription : IUrsItem
    {
        /// <summary>
        /// the urs version of change
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Affected sections of change
        /// </summary>
        public string Section { get; set; }

        [NamesInUrs("Description of Changes")]
        public IEnumerable<string> Changes { get; set; }

        public override string ToString() => $"{Version} {Section}";
    }
}
