﻿using UrsWorker.UrsModel.Attributes;

namespace UrsWorker.UrsModel
{
    public class KeywordReplacement : IUrsItem
    {
        [NamesInUrs("Keyword")]
        public string Name { get; set; }

        public string Value { get; set; }

        public override string ToString() => $"{Name}: {Value}";
    }
}
