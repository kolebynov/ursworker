﻿using System.Collections.Generic;
using UrsWorker.UrsModel.Attributes;

namespace UrsWorker.UrsModel
{
    /// <summary>
    /// Data transfer integration
    /// </summary>
    public class DataIntegration : IUrsItem
    {
        [NamesInUrs("Integration Type")]
        public string Type { get; set; }

        [NamesInUrs("Target System")]
        public string TargetSystem { get; set; }

        public override string ToString() => TargetSystem;
    }
}
