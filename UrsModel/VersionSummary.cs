﻿using System;
using UrsWorker.UrsModel.Attributes;

namespace UrsWorker.UrsModel
{
    public class VersionSummary : IUrsItem
    {
        public string Version { get; set; }

        public DateTime Date { get; set; }

        [NamesInUrs("Sections Impacted")]
        public string SectionsImpacted { get; set; }

        public override string ToString() => $"{Version}: {SectionsImpacted}";
    }
}
