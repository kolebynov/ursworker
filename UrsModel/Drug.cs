﻿using UrsWorker.UrsModel.Attributes;
using UrsWorker.UrsModel.Common;

namespace UrsWorker.UrsModel
{
    public class Drug : IUrsItem
    {
        public const int DefaultSiteUnitMultiplier = 1;

        [NamesInUrs("Drug Code")]
        public string Code { get; set; }

        [NamesInUrs("Unblinded Drug Description", "Drug Description", "Non-Serialized Drug Description")]
        public string UnblindedDescription
        {
            get => _unblindedDescription;
            set
            {
                _unblindedDescription = value;

                if (string.IsNullOrEmpty(BlindedDescription))
                {
                    BlindedDescription = value;
                }
            }
        }

        [NamesInUrs("Blinded Drug Description")]
        public string BlindedDescription { get; set; }

        [NamesInUrs("Material Type")]
        public MaterialType MaterialType { get; set; }

        public Blinding Blinding { get; set; }

        [NamesInUrs("Blinding Group")]
        public int? BlindingGroup { get; set; }

        [NamesInUrs("Resupply Group")]
        public int ResupplyGroup { get; set; }

        [NamesInUrs("Shipping Group")]
        public int ShippingGroup { get; set; }

        public int? PackagingGroup { get; set; }

        [NamesInUrs("Site Unit Multiplier")]
        public int SiteUnitMultiplier { get; set; } = DefaultSiteUnitMultiplier;

        public override string ToString() => $"{Code} - {UnblindedDescription}";

        private string _unblindedDescription;
    }
}
