﻿using UrsWorker.UrsModel.Attributes;

namespace UrsWorker.UrsModel
{
    public class Visit : IUrsItem
    {
        [NamesInUrs("Visit Name")]
        public string Name { get; set; }

        [NamesInUrs("Base Visit (Used for Visit Windows)")]
        public string BaseVisit { get; set; }

        [NamesInUrs("Hard Minimum Days")]
        public int? HardMinimumDays { get; set; }

        [NamesInUrs("Soft Minimum Days")]
        public int? SoftMinimumDays { get; set; }

        [NamesInUrs("Expected Days")]
        public int ExpectedDays { get; set; }

        [NamesInUrs("Soft Maximum Days")]
        public int? SoftMaximumDays { get; set; }

        [NamesInUrs("Hard Maximum Days")]
        public int? HardMaximumDays { get; set; }

        [NamesInUrs("Treatment Code")]
        public string TreatmentCode { get; set; }

        [NamesInUrs("Treatment Description")]
        public string TreatmentDescription { get; set; }

        [NamesInUrs("Drug Code")]
        public string DrugCode { get; set; }

        [NamesInUrs("Unblinded Drug Description", "Drug Description")]
        public string UnblindedDrugDescription { get; set; }

        [NamesInUrs("Quantity")]
        public int Quantity { get; set; }

        [NamesInUrs("Is Visit Skippable?", "Is Visit Skippable")]
        public bool IsSkippable { get; set; }

        [NamesInUrs("Do Not Dispense Days (DND)")]
        public int DndDays { get; set; }

        public override string ToString() => Name;
    }
}
