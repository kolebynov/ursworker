﻿using System;
using System.Collections.Generic;
using UrsWorker.UrsModel.Sections;

namespace UrsWorker.UrsModel
{
    public interface IUrsModel
    {
        string Protocol { get; }
        string Version { get; }
        string IrtVersion { get; }
        DateTime Date { get; }
        bool IsRandomizedStudy { get; }
        IReadOnlyList<VersionSummary> VersionSummaries { get; }
        IReadOnlyList<KeywordReplacement> KeywordReplacements { get; }
        IReadOnlyList<Drug> Drugs { get; }
        IReadOnlyList<Treatment> Treatments { get; }
        IReadOnlyList<string> UnblindedDataPoints { get; }
        IReadOnlyList<VisitSchedule> VisitSchedules { get; }
        IReadOnlyList<StudyParameter> StudyParameters { get; }
        IReadOnlyList<Country> Countries { get; }
        IReadOnlyList<DataIntegration> DataIntegrations { get; }
        IReadOnlyList<Section> Sections { get; }
    }
}
