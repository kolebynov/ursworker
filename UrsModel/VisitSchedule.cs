﻿using System.Collections.Generic;

namespace UrsWorker.UrsModel
{
    public class VisitSchedule
    {
        public string Name { get; set; }
        public IEnumerable<Visit> Visits { get; set; }

        public override string ToString() => Name;
    }
}
